/*
 * Header file for HotDeal Recommender Engine 
 *
 * @author: YuanChengzhi
 * @email: yuanchengzhi@b5m.com
 * @modification time: 2:01am, May 21, 2016
 *
 *
 *
 *
 */

#ifndef HOTDEALRECENGINE_H_
#define HOTDEALRECENGINE_H_

#include <cmath>
#include <algorithm> 

#include <boost/filesystem.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/unordered_map.hpp>

#include "utils/CassPool.h"
#include "utils/rs_content_profile/profiling_content_sim.hpp"

#include "utils/PropSharedLock.h"

#include "utils/TimingHelper.h"

class HotDealRecEngine : public PropSharedLock  
{
	public:
	
	        //Dummy constructor for initialing before swapping; no use otherwise
		HotDealRecEngine()
		{
			//std::cout << "Dummy hotdeal recommender constructor called!\n";
		        LOG(INFO) << "Dummy hotdeal recommender constructor called!\n";
		}
	
		// Defualt constructor IS hereby implemented and encouraged to use as a priority
		// Will get src data from Cassandra
		HotDealRecEngine(const std::string& cassandra_cluster_addr);
		
		// Constructor that inits from resDir
		// Used for resuming from an interruption/upgrading; 
                // will load exisiting results
		HotDealRecEngine(const std::string& cassandra_cluster_addr, 
				 const std::string& resource_dir);
		
		~HotDealRecEngine()
		{
                     //std::cout << "HotDealRecEngine Destructor Called.\n";
                     LOG(INFO) << "HotDealRecEngine Destructor Called.\n";
		}

		/************* Methods for new items streamed from kafka **********/
		
		// checking established itemidIndex_ to see if item exists
		bool is_item_new(const std::string& itemid) const 
                {
		     
                     Indexer_t::right_const_iterator it = itemidIndex_.right.find(itemid);

		     // check new itemidindex from streamed kafka products too 
                     Indexer_t::right_const_iterator it_stream = itemidIndexStream_.right.find(itemid);

		     if(it == itemidIndex_.right.end() && it == itemidIndexStream_.right.end())
		     {
			     //std::cout << itemid << " is new!\n";
			     LOG(INFO) << "From kakfa, itemid: " << itemid << " is a new item.";
			     return true;
                     }

		     //std::cout << itemid << " exists already!\n";
		     return false;
		}

		bool insert_new_item(const std::string& itemid, const std::string& title);

		// if an "-D" document comes in, set an item's status to false from isListedStatusStream and isListedStatus(current estabilished one)
		void update_listed_status(const std::string& itemid);

		/************* End of Methods for new items streamed from kafka **********/

		// based on userid; compute and return in near-real-time
                std::string get_recs_by_userid(const std::string& userid, int topKCount = 50);

                // based on item-item-matrix
		std::string get_recs_by_itemid(const std::string& itemid, int topKCount = 50);
       
	
		void flush_to_disk() const
		{
		      std::string folder_stem = izeneUtil::getCurrentTimestampLocal();

		      std::string master_folder = "../resource/" + folder_stem;
                      //std::cout << "result master folder is: " << master_folder << "\n";

                      boost::filesystem::path master_path(master_folder);

		      if(!boost::filesystem::exists(master_path))
                          boost::filesystem::create_directory(master_path);

		      // flush item_mutual_matrix
		      std::string item_mutal_matrix_filepath = master_folder + "/item_mutual_matrix.tsv";
		      flush_item_mutual_matrix_(item_mutal_matrix_filepath);

		      // flush itemidIndex_
		      std::string itemidIndex_filepath = master_folder + "/itemid_index.tsv";
		      flush_indexer_(itemidIndex_, itemidIndex_filepath);

		      // flush useridIndex_
		      std::string useridIndex_filepath= master_folder + "/userid_index.tsv";
		      flush_indexer_(useridIndex_, useridIndex_filepath);

                      // flush productIsListedStatus_
		      std::string product_islisted_status_filepath = master_folder + "/product_islisted.tsv";
                      flush_product_listed_status_(product_islisted_status_filepath);

		      // flush productTitleTokens_
		      std::string product_title_tokens_filepath = master_folder + "/product_title_tokens.tsv";
		      flush_product_title_tokens_(product_title_tokens_filepath);

		}
		

	private:
		ScoreMatrix_t userItemRatingMatrix_;
		ScoreMatrix_t itemMutualRatingMatrix_;
                
		// indexed userids and itemids
		Indexer_t useridIndex_;
		Indexer_t itemidIndex_;

		// product list extracted from Cassandra
		// As of 11am, May 20, 2016, it only contains product titles
		std::vector<std::string> productData_;
		
		// product titles' tokens 
		std::vector<std::vector<std::string> > productTitleTokens_;
           
                
		/******** Data structure for new streaming items from kafka *****/
		// New itemidIndex_ for new streaming items from Kafka
		int new_item_index_; 
		Indexer_t itemidIndexStream_;

                // simply product id(indexed itemid); for new tiems from kafka
                std::vector<int> itemidIndexListStream_;
 
		// product titles' tokens for streaming new items 
		std::vector<std::vector<std::string> > productTitleTokensStream_;

		// new streamed product status; whether or not it's still listed
		std::map<int, bool> productIsListedStatusStream_;

		/********End of Data Structure for new streaming items from kafka ******/


                // simply product id(indexed itemid); in the same order with productData_
                std::vector<int> itemidIndexList_;
      
		// product status; whether or not it's still listed
		std::map<int, bool> productIsListedStatus_;

		// keeping user avg ratings; userid_index:score
                std::map<int, score_t> userAvgRatingMap_;
                 
                // for the result: item-item matrix recommendations; itemid -> [recommended_itemid0:score0,...]+
		// score0 > score1 > ...
                boost::unordered_map<std::string, std::string> item2ItemRecsMap_;

		// N days' history of screening from Cassandra 
		std::string timeScreeningStr_;

                /**** Helper external classes and methods  **********/

                // Cassandra encapsuled session; Managed by Shared Ptr 
                boost::shared_ptr<izeneUtil::CassPool> pCassPool_;

                // User Profile and Content-based similarity computing
                boost::shared_ptr<ProfileCreate> pContAndUsrPrf_;  

	        //Compute hybrid symilarity matrix for both itemCF and content-based  
		void computeSimilarity_();

	        //load to precalculated, isListed-filtered item-item recommendation list
		void loadToitem2ItemRecsMap_(int topKCount = 80);

                // get sorted DESC, topk from rating pair container 
		std::string getTopKFromRatingPairContainer_(RatingPairContainer_t& item_rating_list, int topKCount);
	
		//rating_pairs: item1:score,item2:score2
		bool load_to_rating_pair_container_(const std::vector<std::string>& rating_pairs, 
                		                    const Indexer_t& itemid_indexer,
		                 		    std::map<int, bool>& product_status,
				                    RatingPairContainer_t& rating_pair_container);

                // In case a new item is asking for item-item recommendation but could find any in current hybrid itemcf and content-based matrx
		// To get a new computing routine from itemidIndex_ and itemidIndexStream_ (doesn't require to be in any of these 2 indices)
		std::string getPureContentSimItemRec_(const std::string& itemid, int topKCount);
			
		// useridIndex_ or itemidIndex_
		void flush_indexer_(const Indexer_t& indexer,
			       	    const std::string& dest_filepath) const 
		{

	             std::ofstream outFile(dest_filepath.c_str());

		     for(Indexer_t::left_const_iterator it = indexer.left.begin();
				                        it != indexer.left.end();
					                ++it)
			     outFile << it->first << "\t" << it->second << '\n';
		}


                // flush to disks for recommender serive upgrading etc
		void flush_item_mutual_matrix_(const std::string& dest_filepath) const 
		{

	             std::ofstream outFile(dest_filepath.c_str());

	             // dimension
                     int matrixDim = itemMutualRatingMatrix_.size();
		     for(int i = 0; i < matrixDim; ++i)
		     { 
			 for(int j = 0; j < matrixDim; ++j)
				 outFile << itemMutualRatingMatrix_[i][j] << "\t";

			 outFile << '\n';
 
		     }

		}

		// flush to disks the productIsListed Status
		void flush_product_listed_status_(const std::string& dest_filepath) const 
		{
		     std::ofstream outFile(dest_filepath.c_str());

		     for(std::map<int, bool>::const_iterator it = productIsListedStatus_.begin(); 
				                             it != productIsListedStatus_.end();
						             ++it)
		     {
                            outFile << it->first << "\t";

			    int boolValue = it->second ? 1 : 0;
			    outFile << boolValue << "\n";
		     }

		}

		// flush to disks the productTokens; in strict order 
                void flush_product_title_tokens_(const std::string& dest_filepath) const 
		{
                     std::ofstream outFile(dest_filepath.c_str());

		     for(std::vector<std::vector<std::string> >::const_iterator it = productTitleTokens_.begin();
				                                                it != productTitleTokens_.end();
									        ++it)
		     {
                           for(std::vector<std::string>::const_iterator it_t = (*it).begin(); it_t != (*it).end(); ++it_t)
			   {
				   outFile << *it_t << "\t";
			   }

			   outFile << "\n";
		     }


		}
       

};

#endif 
