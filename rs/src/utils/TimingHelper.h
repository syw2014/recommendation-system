/*
 *
 * A helper utility for getting timestamps, in strings, whatnot
 *
 * @author: YuanChengzhi
 * @Email: yuanchengzhi@b5m.com
 * @Last Modified: 2:19PM, May 26, 2016
 *
 *
 *
 *
 */ 

#ifndef TIMINGHELPER_H_
#define TIMINGHELPER_H_	

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <string> 

namespace izeneUtil
{


      static std::string getNdaysPrevUTC(int n)
      {

	    if(n < 1)
		return std::string("");

           boost::posix_time::ptime current_utc_time(boost::posix_time::second_clock::universal_time());

	   std::string now_iso_ext_str(boost::posix_time::to_iso_extended_string(current_utc_time));
       
       	   // Always 2016-05-26T09:29:28
	   std::string today_str = now_iso_ext_str.substr(0, 10);
            
	   //UTC today in gregorian
	   boost::gregorian::date today(boost::gregorian::from_string(today_str));

	   boost::gregorian::date old_day = today - boost::gregorian::days(n);

	   std::string old_day_str(boost::gregorian::to_iso_extended_string(old_day));

	   std::string result = old_day_str + " ";

	   //get UTC time half 
	   result += now_iso_ext_str.substr(11);

	   //Cassandra needed 
	   result += "+0000";

           return result;
      }


      // return example: 20160526092928
      static std::string getCurrentTimestampUTC()
      {

           boost::posix_time::ptime current_utc_time(boost::posix_time::second_clock::universal_time());

	   std::string now_iso_str(boost::posix_time::to_iso_string(current_utc_time));
      
           std::size_t pos = now_iso_str.find("T");
           
	   //Get rid of "T" 
	   if(pos != std::string::npos)
               now_iso_str.erase(pos, 1);

           return now_iso_str;
      }

      // return example: 20160526092928
      static std::string getCurrentTimestampLocal()
      {

           boost::posix_time::ptime current_local_time(boost::posix_time::second_clock::local_time());

	   std::string now_iso_str(boost::posix_time::to_iso_string(current_local_time));
      
           std::size_t pos = now_iso_str.find("T");
           
	   //Get rid of "T" 
	   if(pos != std::string::npos)
               now_iso_str.erase(pos, 1);

           return now_iso_str;
      }


      // input example: 20160526092928
      // compare two time strings, can decide if the 1st one is later than second one
      static bool isRightTimeLaterThanLeft(const std::string& time_str_first, const std::string& time_str_second)
      {

          if(time_str_first.empty() || time_str_second.empty())
		  return false;

	  const char* dT = "T";

	  std::string time_first_iso = time_str_first;
	  time_first_iso.insert(8, dT);

	  std::string time_second_iso = time_str_second;
	  time_second_iso.insert(8, dT);

	  // left param
	  boost::posix_time::ptime tl(boost::posix_time::from_iso_string(time_first_iso));

	  // right param
	  boost::posix_time::ptime tr(boost::posix_time::from_iso_string(time_second_iso));

	  return tr > tl; 
      } 

}

#endif
