/*
 * An encapsulated class for Cassandra connector and pool
 * 
 * Purpose: keep-alive session with different methods, initializing hotdeal recommender's data source from online cassandra database 
 *
 * A session is tied to a instantitiated object - all methods will share this session;
 * 
 * Largely based on cassandra-cpp driver example 
 * 
 *
 * @author: Yuanchengzhi
 * @email: yuanchengzhi@b5m.com
 * @last modified: 5:05pm, May 23, 2016
 *
 *
 *
 */


#ifndef CASSPOOL_H_
#define CASSPOOL_H_


#include "cassandra.h"
#include "common.h"

namespace izeneUtil
{

   class CassPool 
   {
       private:
            CassFuture* connect_future;
            CassCluster* cluster;
            CassSession* session;



      public: 
            
            CassPool(const char* cluster_addr)
            {
              // set up and connect to cluster 
              connect_future = NULL;
              cluster = cass_cluster_new();
              session = cass_session_new();

              // Add contact points                 
              cass_cluster_set_contact_points(cluster, cluster_addr);

              // Provide the cluster object as configuration to connect the session 
              connect_future = cass_session_connect(session, cluster);

              if (cass_future_error_code(connect_future) != CASS_OK)
              {
                   //std::cout << "CassPool Constructor: Connection to cassandra failed!\n";
                  
                   cass_future_free(connect_future);
                   cass_cluster_free(cluster);
                   cass_session_free(session);
              }

              //std::cout << "CassPool Successfully Connected!\n";

            }

           ~CassPool()
            {
                   cass_future_free(connect_future);
                   cass_cluster_free(cluster);
                   cass_session_free(session);
               
                   //std::cout << "CassPool Destructor Called!\n";            
                   LOG(INFO) << "CassPool Destructor Called!\n";            
            }    


bool load_from_product_db(const char* query_statement_str,
                          Indexer_t& itemid_index,
                          std::map<int, bool>& product_status,
			  std::vector<int>& itemid_index_list,
                          std::vector<std::string>& product_title_list) 
{
        //	std::cout << "started loading from product db!\n";

        CassStatement* statement = cass_statement_new(query_statement_str, 0);
        
        CassFuture* result_future = cass_session_execute(session, statement);
        
        if(cass_future_error_code(result_future) == CASS_OK)
        {
             const CassResult* result = cass_future_get_result(result_future);
             CassIterator* rows = cass_iterator_from_result(result);
             
             int index = 0;
          
             while(cass_iterator_next(rows))
             {
                 const CassRow* row = cass_iterator_get_row(rows);
             
                 // load itemid to indexer
                 const CassValue* value = cass_row_get_column_by_name(row, "docid");
                 
                 const char* value_str;
                 size_t value_str_len;

                 cass_value_get_string(value, &value_str, &value_str_len);
                 
                 std::string itemid_entry = value_str;

		 itemid_entry = itemid_entry.substr(0, value_str_len);
                
                 itemid_index.insert(Indexer_t::value_type(index, itemid_entry));

		 // load to produc_status; only active product's status will be kept as true; 
                 const CassValue* value_status = cass_row_get_column_by_name(row, "status");
                 
                 cass_bool_t status_entry;
		 cass_value_get_bool(value_status, &status_entry);


		 if(status_entry == true)
			 product_status[index] = true;


		 // add to itemid_index_list 
                 itemid_index_list.push_back(index);
        
                 // load titles  
                 const CassValue* value_title = cass_row_get_column_by_name(row, "title");
                 
                 const char* value_title_str;
                 size_t value_title_str_len;

                 cass_value_get_string(value_title, &value_title_str, &value_title_str_len);
                 
                 std::string title_entry = value_title_str;
		 title_entry = title_entry.substr(0, value_title_str_len);
                 // std::cout << title_entry << "aaa\n";

		 product_title_list.push_back(title_entry);
                 
                 ++index;
             }
 
             cass_iterator_free(rows);            

             cass_result_free(result);

             cass_statement_free(statement);
             cass_future_free(result_future);

             return true;
       }     

       //Error Zone; don't really care what error msg is; print out anyway 
       const char* err_msg;
       size_t msg_len;
       cass_future_error_message(result_future, &err_msg, &msg_len);
       //std::cout << "Unable to run query due to: " << err_msg << '\n';
       
       //Free resource 
       cass_statement_free(statement);
       cass_future_free(result_future);
       
       return false;

}



 
bool load_from_uniq_userid_db(const char* query_statement_str,
                                        Indexer_t& userid_index)
{
	//std::cout << "started loading from uniq userid db!\n";

        CassStatement* statement = cass_statement_new(query_statement_str, 0);
        
        CassFuture* result_future = cass_session_execute(session, statement);
        
        if(cass_future_error_code(result_future) == CASS_OK)
        {
             const CassResult* result = cass_future_get_result(result_future);
             CassIterator* rows = cass_iterator_from_result(result);
             
             int index = 0;
          
             while(cass_iterator_next(rows))
             {
                 const CassRow* row = cass_iterator_get_row(rows);
             
                 // load userid to indexer
                 const CassValue* value = cass_row_get_column_by_name(row, "userid");
                 
                 const char* value_str;
                 size_t value_str_len;

                 cass_value_get_string(value, &value_str, &value_str_len);
                 
                 std::string userid_entry = value_str;

		 userid_entry = userid_entry.substr(0, value_str_len);
               
		 //std::cout << "userid: " << userid_entry << '\n';
		  
                 userid_index.insert(Indexer_t::value_type(index, userid_entry));

                 ++index;

             }
 
             cass_iterator_free(rows);            

             cass_result_free(result);

             cass_statement_free(statement);
             cass_future_free(result_future);

             return true;
       }     

       //Error Zone; don't really care what error msg is; print out anyway 
       const char* err_msg;
       size_t msg_len;
       cass_future_error_message(result_future, &err_msg, &msg_len);
       //std::cout << "Unable to run query due to: " << err_msg << '\n';
       
       //Free resource 
       cass_statement_free(statement);
       cass_future_free(result_future);
       
       return false;


}

// load to useridIndex_ from user_behavior_item_cqs; dedup needed; filtering by N days needed 
bool load_to_userid_index(const char* query_statement_str,
                                        Indexer_t& userid_index)
{
	//std::cout << "started loading to userididIndx_!\n";

        CassStatement* statement = cass_statement_new(query_statement_str, 0);
        
        CassFuture* result_future = cass_session_execute(session, statement);
        
        if(cass_future_error_code(result_future) == CASS_OK)
        {
             const CassResult* result = cass_future_get_result(result_future);
             CassIterator* rows = cass_iterator_from_result(result);
             
             int index = 0;
          
             while(cass_iterator_next(rows))
             {
                 const CassRow* row = cass_iterator_get_row(rows);
             
                 // load userid to indexer
                 const CassValue* value = cass_row_get_column_by_name(row, "user_behavior_cq_userid");
                 
                 const char* value_str;
                 size_t value_str_len;

                 cass_value_get_string(value, &value_str, &value_str_len);
                 
                 std::string userid_entry = value_str;

		 userid_entry = userid_entry.substr(0, value_str_len);
               
		 //std::cout << "userid: " << userid_entry << '\n';

		 Indexer_t::right_const_iterator it = userid_index.right.find(userid_entry);

		 // if a userid's already indexed 
		 if(it != userid_index.right.end())
			 continue;
		  
                 userid_index.insert(Indexer_t::value_type(index, userid_entry));

                 ++index;

             }
 
             cass_iterator_free(rows);            

             cass_result_free(result);

             cass_statement_free(statement);
             cass_future_free(result_future);

             return true;
       }     

       //Error Zone; don't really care what error msg is; print out anyway 
       const char* err_msg;
       size_t msg_len;
       cass_future_error_message(result_future, &err_msg, &msg_len);
       //std::cout << "Unable to run query due to: " << err_msg << '\n';
       
       //Free resource 
       cass_statement_free(statement);
       cass_future_free(result_future);
       
       return false;


}

bool load_from_user_trail(const char* query_statement_str,
                                   Indexer_t& userid_index_map,
                                   Indexer_t& itemid_index_map,
                                   ScoreMatrix_t& user_item_rating_matrix)
{
        //std::cout << "Started loading from user trail!\n";

        CassStatement* statement = cass_statement_new(query_statement_str, 0);
        
        CassFuture* result_future = cass_session_execute(session, statement);

	size_t itemCount = itemid_index_map.size();
        size_t userCount = userid_index_map.size();

	if (itemCount == 0 || userCount == 0)
		return false;

        std::vector<score_t> linePaddings(itemCount, 0.0);

        for(int i = 0; i < userCount; ++i)
		user_item_rating_matrix.push_back(linePaddings);


        //rating formula: base_score + (1- base_score) * browsedCount / Max_Browsed_Count 
	//1) base score for browsing once; 
	//2) browsed count as a weighted increment
	const score_t Base_score = 0.9;
	const score_t Max_Browsed_Count = 35;
		     
	// every visit gets an 1/Max_Browsed_Count score increment
	score_t visit_incremental_score = (1.0 - Base_score) / Max_Browsed_Count;
	

        if(cass_future_error_code(result_future) == CASS_OK)
        {

             const CassResult* result = cass_future_get_result(result_future);
             CassIterator* rows = cass_iterator_from_result(result);

             while(cass_iterator_next(rows))
             {

                 //getting userid 
                 const CassRow* row = cass_iterator_get_row(rows);
                 const CassValue* value = cass_row_get_column_by_name(row, "user_behavior_cq_userid");
                 
                 const char* value_str;
                 size_t value_str_len;

                 cass_value_get_string(value, &value_str, &value_str_len);
                 
                 std::string userid_entry = value_str;
                 userid_entry = userid_entry.substr(0, value_str_len);

		 //std::cout << "userid: " << userid_entry << ", ";

                 //get index for userid
		 Indexer_t::right_const_iterator it_usr = userid_index_map.right.find(userid_entry);
		 if(it_usr == userid_index_map.right.end())
			 continue;

		 //userid_index: 0, 1, 2,
		 int userid_index_i = it_usr->second;
    
		 //getting itemid from the same row 
                 const CassValue* value_itemid = cass_row_get_column_by_name(row, "itemid");
                 
                 const char* value_itemid_str;
                 size_t value_itemid_str_len;

                 cass_value_get_string(value_itemid, &value_itemid_str, &value_itemid_str_len);
                 
                 std::string itemid_entry = value_itemid_str;
                 itemid_entry = itemid_entry.substr(0, value_itemid_str_len);

		 //std::cout << "itemid: " << itemid_entry << " ---> ";

                 //get index for itemid
		 Indexer_t::right_const_iterator it_item = itemid_index_map.right.find(itemid_entry);
		 if(it_item == itemid_index_map.right.end())
			 continue;

                 // std::cout << "itemid_index Found!\n\n";		 
		 //itemid_index: 0, 1, 2
		 int itemid_index_i = it_item->second;

		 score_t previous_score = user_item_rating_matrix[userid_index_i][itemid_index_i];
                 
		 // first visit
		 if(previous_score < Base_score)
                     user_item_rating_matrix[userid_index_i][itemid_index_i] = Base_score + visit_incremental_score;
		 else 
		 { 
                     user_item_rating_matrix[userid_index_i][itemid_index_i] += visit_incremental_score;
                   
		     // capping score to 1.0
		     if(user_item_rating_matrix[userid_index_i][itemid_index_i] > 1.0)
			     user_item_rating_matrix[userid_index_i][itemid_index_i] = 1.0;

		 }

		 //std::cout << "score: " << user_item_rating_matrix[userid_index_i][itemid_index_i] << "\n\n";

             }
 
             cass_iterator_free(rows);            

             cass_result_free(result);

             cass_statement_free(statement);
             cass_future_free(result_future);

             return true;
       }     

       //Error Zone; don't really care what error msg is; print out anyway 
       const char* err_msg;
       size_t msg_len;
       cass_future_error_message(result_future, &err_msg, &msg_len);
       //std::cout << "Unable to run query due to: " << err_msg << '\n';
       
       //Free resource 
       cass_statement_free(statement);
       cass_future_free(result_future);
       
       return false;

}


int table_row_count(const char* query_statement_str) 
{

	//std::cout << "started loading from tab row count!\n";

        CassStatement* statement = cass_statement_new(query_statement_str, 0);
        
        CassFuture* result_future = cass_session_execute(session, statement);
        
        if(cass_future_error_code(result_future) == CASS_OK)
        {
             const CassResult* result = cass_future_get_result(result_future);
             
             int row_count = cass_result_row_count(result);


             cass_result_free(result);


             cass_statement_free(statement);
             cass_future_free(result_future);

             return row_count;
       }     


       //Error Zone; don't really care what error msg is; print out anyway 
       const char* err_msg;
       size_t msg_len;
       cass_future_error_message(result_future, &err_msg, &msg_len);
       std::cout << "Unable to run query due to: " << err_msg << '\n';
       
       //Free resource 
       cass_statement_free(statement);
       cass_future_free(result_future);
       
       return -1;
}


bool get_user_history(const char* query_statement_str,
                         std::vector<std::string>& user_history)
{
	//std::cout << "Started fetching user history!\n";

        CassStatement* statement = cass_statement_new(query_statement_str, 0);
        
        CassFuture* result_future = cass_session_execute(session, statement);
        
        if(cass_future_error_code(result_future) == CASS_OK)
        {
             const CassResult* result = cass_future_get_result(result_future);
             CassIterator* rows = cass_iterator_from_result(result);

             while(cass_iterator_next(rows))
             {
                 const CassRow* row = cass_iterator_get_row(rows);
                 const CassValue* value = cass_row_get_column_by_name(row, "itemid");
                 
                 const char* value_str;
                 size_t value_str_len;

                 cass_value_get_string(value, &value_str, &value_str_len);
                 
                 std::string history_entry = value_str;
                 history_entry = history_entry.substr(0, value_str_len);

		 user_history.push_back(history_entry);
                 

             }
 
             cass_iterator_free(rows);            

             cass_result_free(result);

             cass_statement_free(statement);
             cass_future_free(result_future);

             return true;
       }     

       //Error Zone; don't really care what error msg is; print out anyway 
       const char* err_msg;
       size_t msg_len;
       cass_future_error_message(result_future, &err_msg, &msg_len);
       //std::cout << "Unable to run query due to: " << err_msg << '\n';
       
       //Free resource 
       cass_statement_free(statement);
       cass_future_free(result_future);
       
       return false;

}


};

// end of namespace izeneUtil
}

#endif

