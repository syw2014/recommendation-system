/*************************************************************************
   @ Created by Xiaofeng Yue on 4:55pm, Dec 18, 2015
   @ Mail: yuanchengzhi@b5m.com
   
   @ Last modified: 10:20am, May 31, 2016
 
 ************************************************************************/

#include <glog/logging.h>
#include <boost/filesystem.hpp>

/*********glog initialize**********/
static void InitGlog(const char* cProgram, const char* logDir){

	if(!boost::filesystem::exists(logDir)){
		boost::filesystem::create_directory(logDir);
	}

	char cInfoPath[100];
	char cErrPath[100];
	char cWarnPath[100];
	char cFatalPath[100];

	//set log path
	snprintf(cInfoPath, sizeof(cInfoPath), "%s%s", logDir, "/INFO_");
	snprintf(cErrPath, sizeof(cErrPath), "%s%s", logDir, "/ERROR_");
	snprintf(cWarnPath, sizeof(cWarnPath), "%s%s", logDir, "/WARNING_");
	snprintf(cFatalPath, sizeof(cFatalPath), "%s%s", logDir, "/FATAL_");

	google::InitGoogleLogging(cProgram);

	FLAGS_logbufsecs = 0; // no cache
	FLAGS_stop_logging_if_full_disk = true; // no writen when disk is full
	FLAGS_alsologtostderr = false; // close to stderr
        FLAGS_max_log_size = 2048; //max size 2G

	google::SetLogDestination(google::GLOG_INFO,cInfoPath);
	google::SetLogDestination(google::GLOG_ERROR,cErrPath);
	google::SetLogDestination(google::GLOG_WARNING,cWarnPath);
	google::SetLogDestination(google::GLOG_FATAL,cFatalPath);
}

