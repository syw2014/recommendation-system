#ifndef KAFKA_DRIVER_H_
#define KAFKA_DRIVER_H_

#include <stdlib.h>
#include <fstream>

#include <csignal>
#include <iostream>
#include <map>

//#include "librdkafka/rdkafkacpp.h"

#include <librdkafka/rdkafkacpp.h>
#include "concurrent_queue.h"

#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/functional/hash.hpp>

//#include "json/json.h"

//namespace fs = boost::filesystem;
static bool run;// = true;
static bool exit_eof;// = false;

bool isNum(const std::string& str){
    std::stringstream sin(str);
    double d;
    char p;
    if(!(sin >> d))
        return false;
    if(sin >> p)
        return false;
    return true;
}


//class ExampleEventCb : public RdKafka::EventCb {
class ExampleEventCb : public RdKafka::EventCb {
    public:
        void event_cb (RdKafka::Event &event) {
            switch (event.type())
            {
                case RdKafka::Event::EVENT_ERROR:
                    std::cerr << "ERROR (" << RdKafka::err2str(event.err()) << "): " <<
                        event.str() << std::endl;
                    if (event.err() == RdKafka::ERR__ALL_BROKERS_DOWN)
                        run = false;
                    break;

                case RdKafka::Event::EVENT_STATS:
                    std::cerr << "\"STATS\": " << event.str() << std::endl;
                    break;

                case RdKafka::Event::EVENT_LOG:
                    fprintf(stderr, "LOG-%i-%s: %s\n",
                            event.severity(), event.fac().c_str(), event.str().c_str());
                    break;

                default:
                    std::cerr << "EVENT " << event.type() <<
                        " (" << RdKafka::err2str(event.err()) << "): " <<
                            event.str() << std::endl;
                    break;
            }
        }
};

class ExampleOffsetCommitCb : public RdKafka::OffsetCommitCb {
 public:
  void offset_commit_cb (RdKafka::ErrorCode err, std::vector<RdKafka::TopicPartition*> &offsets) {
    std::cerr << ": Propagate offset for " << offsets.size() << " partitions, error: " << RdKafka::err2str(err) << std::endl;

    /* No offsets to commit, dont report anything. */
    if (err == RdKafka::ERR__NO_OFFSET)
      return;

    std::cout << "{ " <<
        "\"name\": \"offsets_committed\", " <<
        "\"success\": " << (err ? "false" : "true") << ", " <<
        "\"error\": \"" << (err ? RdKafka::err2str(err) : "") << "\", " <<
        "\"offsets\": [ ";
    assert(offsets.size() > 0);
    for (unsigned int i = 0 ; i < offsets.size() ; i++) {
      assert(err != 0 || offsets[i]->offset() > -1);
      std::cout << (i == 0 ? "" : ", ") << "{ " <<
          " \"topic\": \"" << offsets[i]->topic() << "\", " <<
          " \"partition\": " << offsets[i]->partition() << ", " <<
          " \"offset\": " << (int)offsets[i]->offset() << ", " <<
          " \"error\": \"" <<
          (offsets[i]->err() ? RdKafka::err2str(offsets[i]->err()) : "") <<
          "\" " <<
          " }";
    }
    std::cout << " ] }" << std::endl;
  }
};

class ExampleRebalanceCb : public RdKafka::RebalanceCb {
private:
  static void part_list_json (const std::vector<RdKafka::TopicPartition*> &partitions) {
    for (unsigned int i = 0 ; i < partitions.size() ; i++)
      std::cout << (i==0?"":", ") << "{ " <<
	" \"topic\": \"" << partitions[i]->topic() << "\", " <<
	" \"partition\": " << partitions[i]->partition() <<
	" }";
  }
 public:
  void rebalance_cb (RdKafka::KafkaConsumer *consumer,
		     RdKafka::ErrorCode err,
		     std::vector<RdKafka::TopicPartition*> &partitions) {
    std::cout << "{ " <<
      "\"name\": \"partitions_" << (err == RdKafka::ERR__ASSIGN_PARTITIONS ?
				    "assigned" : "revoked") << "\", " <<
      "\"partitions\": [ ";
    part_list_json(partitions);
    std::cout << "] }" << std::endl;

    if (err == RdKafka::ERR__REVOKE_PARTITIONS)
      consumer->unassign();
    else
      consumer->assign(partitions);
  }
};

static void sigterm(int sig) 
{
    run = false;
}
//class KafkaDriver : public RdKafka::ConsumeCb
class KafkaDriver 
{
    private:
        boost::shared_ptr<RdKafka::Conf> conf_;// = RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL);
        boost::shared_ptr<RdKafka::Conf> tconf_;// = RdKafka::Conf::create(RdKafka::Conf::CONF_TOPIC);
        boost::shared_ptr<RdKafka::Topic> topic_handle_;
        boost::shared_ptr<RdKafka::Consumer> consumer_handle_;

        std::map<std::string, std::string> conf_item_vec_;
        
        std::ofstream ofs;
        //kafka conf
        std::string brokers_;   //hosts
        std::string topic_;
        std::string group_id_; //kafka group id

        std::string err_str_; // record error info

        int32_t partition_;// = RdKafka::Topic::PARTITION_UA;; //kafka partition
        int64_t offset_;// = RdKafka::Topic::OFFSET_BEGINNING;
        bool use_ccb_;// = false;
        std::string offset_last_;
        // test
        //izenelib::util::concurrent_queue<std::string>* msgQueue_;
	boost::shared_ptr<izenelib::util::concurrent_queue<std::string> > msgQueue_;
        boost::thread* work_thread_;
    public:
        KafkaDriver(const std::string& conf_file, const std::string& work_mode, boost::shared_ptr<izenelib::util::concurrent_queue<std::string> > que)
            :offset_(RdKafka::Topic::OFFSET_BEGINNING) ,use_ccb_(false), msgQueue_(que){
            
                conf_.reset(RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL));
                tconf_.reset(RdKafka::Conf::create(RdKafka::Conf::CONF_TOPIC));
                partition_ = RdKafka::Topic::PARTITION_UA; // kakfa partition
                //msgQueue_ = new izenelib::util::concurrent_queue<std::string>(10000);
               // work_thread_ = new boost::thread(boost::bind(&KafkaDriver::print, this));
                // others
                run = true;
                exit_eof = false;

                size_t ret = load_conf_properties(conf_file);
                if(0 != ret){
                    std::cout << "load configure file failed!\n";
                }
                
                // get the last offset
                std::ifstream ifs("../conf/offset");
                std::string line;
                while(getline(ifs, line)){
                    offset_last_ = line;
                }
                ifs.close();

                //open offset file
                ofs.open("../conf/offset");
                
                
                if(0 == work_mode.compare("consumer")){
                    // other process
                    ret = set_consumer_properties();
                    if(0 != ret){
                        std::cout << "set consumer properties failed!\n";
                    }
                    ret = consumer_new();
                    if(0 != ret){
                        std::cout << "create new consumer failed!\n";
                    }
                    ret = consumer_run();
                    if(0 != ret){
                        std::cout << "run consumer failed!\n";
                    }
                }else if(0 == work_mode.compare("producer")){
                    std::cout << "Start producer mode!\n";
                }else{
                    std::cout << "Unknow work mode\n";
                }
        }

        ~KafkaDriver(){
            // Stop consumer
            consumer_handle_->stop(topic_handle_.get(), partition_);
            consumer_handle_->poll(1000);
            ofs.close();
        }
        // MESSAGE PROCESS
       /* void print(){
            while(true){
                std::string data;
                msgQueue_->pop(data);
                std::cout << "TEST: " << msgQueue_->size() <<"\t" <<data << std::endl;
            }
        }*/

        void msg_consume(RdKafka::Message* message, void* opaque){
            std::string msg_str("");
            switch(message->err()){
            case RdKafka::ERR__TIMED_OUT:
                break;
            
            case RdKafka::ERR_NO_ERROR:
                // Process real message
                //std::cout << "Read msg at offset: " << message->offset() << std::endl;
                ofs << message->offset() << std::endl;
                if(message->key()){
                    std::cout << "Key: " << *message->key() << std::endl;
                }
                msg_str = static_cast<const char*>(message->payload());
               
		msg_str = msg_str.substr(0, static_cast<int>(message->len()));

		// message parse
                kafka_consume_message_process(msg_str, message->offset());
                break;
            case RdKafka::ERR__PARTITION_EOF:
                if(exit_eof)
                    run = false;
                break;
            case RdKafka::ERR__UNKNOWN_TOPIC:
            case RdKafka::ERR__UNKNOWN_PARTITION:
                std::cerr << "Consume failed: " << message->errstr() << std::endl;
                run = false;
                break;
            default:
                std::cout << "Consume failed: " << message->errstr() << std::endl;
                run = false;
            }
        }
        // Interface
        size_t kafka_consume_message_process(std::string msg_str, int64_t offset){
            msgQueue_->push(msg_str);
            //std::cout << "TEST :" << msgQueue_->size() << "\t" <<msg_str  << std::endl;
            return 0;
        }


        size_t load_conf_properties(const std::string& conf_file){
            std::ifstream read_conf(conf_file.c_str());
            if(!read_conf){
                std::cout << "Open configure file error!\n";
                return -1;
            }
            std::string line;
            while(getline(read_conf, line)){
                //remove all spaces
                for(std::string::iterator iter = line.begin(); iter!=line.end();++iter){
                    if(*iter == ' ')
                        line.erase(iter);
                }
                uint32_t pos = line.find_first_of("=");
                if(pos == std::string::npos)
                    continue;
                std::string key, val;
                key = line.substr(0, pos);
                val = line.substr(pos+1);
                conf_item_vec_.insert(make_pair(key, val));
            }
            read_conf.close();

            return 0;
        }

        size_t set_consumer_properties(){
            // set brokers_, topic_
            brokers_ = conf_item_vec_["kafka_brokers"];
            topic_ = conf_item_vec_["kafka_topic"];
            // set partition
            if(!isNum(conf_item_vec_["kafka_partition"])){
                std::cout << "Partition is not numberical!\n";
                return -1;
            }
            partition_ = std::atoi(conf_item_vec_["kafka_partition"].c_str());

            // set use ccb
            std::string ccbFlag = conf_item_vec_["kafka_ccb"];
            if(0 == ccbFlag.compare("true")){
                use_ccb_ = 1;
            }else if(0 == ccbFlag.compare("false") || ccbFlag.empty()){
                use_ccb_ = 0;
            }else{
                std::cout << "Unknown option for kafka_ccb.\n";
            }

            // set start offset
            std::string offset = conf_item_vec_["kafka_offset"];
            if(0 == offset.compare("local_stored")){
                
                if(!offset_last_.empty()){
                    offset_ = std::atoi(offset_last_.c_str());
                }else{
                    //offset_ = RdKafka::Topic::OFFSET_BEGINNING;
                    offset_ = RdKafka::Topic::OFFSET_END;
                    //std::cout << "Local offset is not exist!\n";
                    //return -1;
                }
            }else if(0 == offset.compare("stored")){
                offset_ = RdKafka::Topic::OFFSET_STORED;
            }else if(0 == offset.compare("default")){
                offset_ = RdKafka::Topic::OFFSET_END;
            }else if(!offset.empty()){
                offset_ = std::atoi(offset.c_str());
            }

            group_id_ = conf_item_vec_["kafka_group"];

            // Set kafka conf
            conf_->set("metadata.broker.list", brokers_, err_str_);
            ExampleEventCb ex_event_cb;
            /*
            conf_->set("event_cb", &ex_event_cb, err_str_);
            ExampleRebalanceCb ex_rebalance_cb;
            conf_->set("rebalance_cb", &ex_rebalance_cb, err_str_);
            ExampleOffsetCommitCb ex_offset_commit_cb;
            conf_->set("offset_commit_cb", &ex_offset_commit_cb, err_str_);
            */
            conf_->set("broker.version.fallback","0.8.2.2", err_str_);
            return 0;
        }

        // RESERVE
        void set_producer_properties(){
        }

        size_t consumer_new(){
            consumer_handle_.reset(RdKafka::Consumer::create(conf_.get(), err_str_));
            if(!consumer_handle_){
                std::cout << "Created consumer failed: " << err_str_ << std::endl;
                return -1;
            }

            std::cout << "% Created consumer " + consumer_handle_->name() << std::endl;
            // Crate topics
            topic_handle_.reset(RdKafka::Topic::create(consumer_handle_.get(), topic_, tconf_.get(), err_str_));
            if(!topic_handle_){
                std::cout << "Created topic failed " << err_str_ << std::endl;
                return -1;
            }
            return 0;
        }

        size_t consumer_run(){
            // Start consumer for topic + partition at start offset
            RdKafka::ErrorCode resp = consumer_handle_->start(topic_handle_.get(), partition_, offset_);
            if(resp != RdKafka::ERR_NO_ERROR){
                std::cout << "Start consumer failed : " << RdKafka::err2str(resp) << std::endl;
                return -1;
            }

            // Consume message
            while(run){
                std::string msg_str;
                if(use_ccb_){
                  //  consumer_handle_->consume_callback(topic_handle_.get(), partition_, 1000, this, &use_ccb_);
                }else{
                    RdKafka::Message* msg = consumer_handle_->consume(topic_handle_.get(), partition_, 1000);
                    msg_consume(msg, NULL);
                    delete msg;
                }
                consumer_handle_->poll(0);
            }
            return 0;
        }

        void consume_cb (RdKafka::Message &msg, void *opaque) {
                msg_consume(&msg, opaque);
            }
};

#endif
