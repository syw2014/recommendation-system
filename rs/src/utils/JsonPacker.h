/*
 * A json packer for input string with format: k1:v1,k2:v2,...
 * 
 *
 * Output: {"k_name1":"k1","v_name1":"v1"},...
 *
 * Example: aaa:2,bbb:1
 * Output:  {"itemid":"aaa","score":"2"},{...}
 *
 * @Author: YuanChengzhi
 * @Email: yuanchengzhi@b5m.com
 * @Last Modified: 8:35pm, June 1st, 2016
 *
 *
 *
 */

#ifndef JSONPACKER_H_
#define JSONPACKER_H_

#include <boost/algorithm/string.hpp>
#include <string>
#include <vector> 

namespace izeneUtil
{
    
	std::string jsonPacker(const std::string& k_name,
			       const std::string& v_name,
			       const std::string& original_kv_str)
	{

		if(original_kv_str.empty())
			return std::string("");

		std::vector<std::string> lineTokens;
		boost::split(lineTokens, original_kv_str, boost::is_any_of(","));
		if(lineTokens.empty())
                        return std::string("");

		//k1:v1
		std::vector<std::string> pairTokens;
            
		std::string result("");

		for(std::vector<std::string>::iterator it = lineTokens.begin(); it != lineTokens.end(); ++it)
		{
                      std::string pair = *it;
		      if(pair.empty())
			    continue;
                     
		      boost::split(pairTokens, pair, boost::is_any_of(":"));
                      if(pairTokens.size() != 2)
			    continue;
                     

		      result += "{\"" + k_name + "\":";
		      result += "\"" + pairTokens[0] + "\",";
		      result += "\"" + v_name + "\":";
		      result += "\"" + pairTokens[1] + "\"},";
		}

		if(result.empty())
			return std::string("");

                result = result.substr(0, result.size() - 1);

		return result;
	}

}

#endif
