/*
 * A common header file for HotDeal Recommender Engine 
 *
 * @author: YuanChengzhi
 * @email: yuanchengzhi@b5m.com
 * @modification time: 10:12am, May 31, 2016
 *
 *
 *
 *
 */

#ifndef COMMON_H_
#define COMMON_H_


#include <map> 
#include <set>
#include <vector>
#include <string>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>

#include <glog/logging.h>

#include <boost/timer.hpp>
#include <boost/bimap.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/unordered_map.hpp>
#include <boost/concept_check.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>


typedef double score_t;
typedef std::vector<std::vector<score_t> > ScoreMatrix_t;

// for indexing of ids; sequential #, userid/itemid
typedef boost::bimap<int, std::string>  Indexer_t;

// item:score
typedef std::vector<std::pair<std::string, score_t> > RatingPairContainer_t;

#endif
