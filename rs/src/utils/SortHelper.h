/*
 *  A helper file for sorting pair<T,P>, enabling std::sort on containers such as vector<pair<T,R> >
 *
 *  @last modified: 5:04pm, May 23, 2016
 *
 *
 */
#ifndef SORTHELPER_H_
#define SORTHELPER_H_

#include <map>

// Sort by second
// NOTE: Typename R muse be comparable
template<typename T, typename R>
class PairSort
{
    public:
        typedef std::pair<T, R> VectorPair;
        
        static bool sortDescendBySecond(const VectorPair& lhs, const VectorPair& rhs){
            return lhs.second > rhs.second;
        }
        
        static bool sortAscendBySecond(const VectorPair& lhs, const VectorPair& rhs){
            return lhs.second < rhs.second;
        }
};

#endif
