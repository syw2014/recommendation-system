/*
 *  simple "ls" program 
 *  -lboost_system -lboost_filesystem
 *
 *  returns the last modified subfolder name under a given master folder  
 */

#ifndef FILESYSHELPER_H_
#define FILESYSHELPER_H_

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>

#include <vector>
#include <string>
#include <algorithm>
#include <map>

#include "SortHelper.h"

namespace izeneUtil
{
     std::string getLastModifiedSubFolder(const std::string& parent_folder)
     {
    
	if(parent_folder.empty())
	      return std::string("");

        std::vector<std::pair<std::string, std::time_t> > fileNameTime;
  
        boost::filesystem::path master_path(parent_folder);

        if(!boost::filesystem::exists(master_path) ||
	   !boost::filesystem::is_directory(master_path))
        {
	      return std::string("");
        }


        boost::filesystem::directory_iterator end_iter;

	for(boost::filesystem::directory_iterator dir_itr(master_path); dir_itr != end_iter; ++dir_itr)
	{
      
	    if(!boost::filesystem::is_directory(dir_itr->status()))
		    continue;

            fileNameTime.push_back(std::make_pair(dir_itr->path().filename().string(), boost::filesystem::last_write_time(dir_itr->path())));
	}

	if(fileNameTime.size() == 0)
	{
	    return std::string("");
	}

	std::sort(fileNameTime.begin(), fileNameTime.end(), PairSort<std::string, std::time_t>::sortDescendBySecond);

	return fileNameTime.begin()->first;
    }

}



#endif 
