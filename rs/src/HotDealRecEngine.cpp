 /*
  * A hybrid recommender engine for hotdeal products.
  *
  * Provides a hybrid recommending algorithm based on itemCF, content-based similarity and user profiling
  *
  * 2 get methods provided:
  * 1) get_recs_by_itemid() returns a list of recommended items based on itemid input, precomputed(updated in a very short period), and responds in real time 
  * 2) get_recs_by_userid() returns a list of recommended items based on userid input, computed and responded in near-real-time 
  *
  *
  * @author: YuanChengzhi
  * @email: yuanchengzhi@b5m.com 
  * @modification time: 8:36pm, May 21, 2016
  *
  *
  */


#include "HotDealRecEngine.h"
#include "utils/SortHelper.h"

const score_t zero_thres = 0.000001;

//Default Constructor; pulling off data from Cassandra only
HotDealRecEngine::HotDealRecEngine(const std::string& cassandra_cluster_addr)
{   

     boost::timer t;

     pCassPool_.reset(new izeneUtil::CassPool(cassandra_cluster_addr.c_str()));
    
     // only get frm hot_product_cqs and user_behavior_item_cqs within N days
     int time_span = 7;
     timeScreeningStr_ = izeneUtil::getNdaysPrevUTC(time_span); 
    
     //std::cout << "History time is: " << timeScreeningStr_ << "\n\n";
     LOG(INFO) << "History time is: " << timeScreeningStr_ << "\n\n";

     /* from product_db, init and load 
      * 1) itemidIndex_, 
      * 2) productIsListedStatus_ 
      * 3) itemidIndexList_
      * 4) productData_  (currently only title) 
      *
      * 3) and 4) are in exact same sequence, used for content-based similarity computing
      */
     std::string query_load_from_product_db = "select * from products_keyspace.hot_product_cqs ";
     query_load_from_product_db += "where status_updated_at > maxTimeuuid(\'";
     query_load_from_product_db += timeScreeningStr_;
     query_load_from_product_db += "\') allow filtering";
     pCassPool_->load_from_product_db(query_load_from_product_db.c_str(),
		                      itemidIndex_,
				      productIsListedStatus_,
				      itemidIndexList_,
				      productData_);
     //std::cout << "Since recommender construction, after loading from product_db took total: " << t.elapsed() << " seconds.\n";
     LOG(INFO) << "Since recommender construction, after loading from product_db took total: " << t.elapsed() << " seconds.\n";

     /** from full user_behavior item table **/
     std::string query_from_user_behavior_item = "select * from products_keyspace.user_behavior_item_cqs ";
     query_from_user_behavior_item += "where id > maxTimeuuid(\'";
     query_from_user_behavior_item += timeScreeningStr_;
     query_from_user_behavior_item += "\') allow filtering";


     //load to useridIndex_; first run
     pCassPool_->load_to_userid_index(query_from_user_behavior_item.c_str(), 
		                      useridIndex_);

     //std::cout << "Done loading to useridIndex_!\n"
               //<< "Since recommender construction, after loading to useridIndex took total: " << t.elapsed() << " seconds.\n";
     
     LOG(INFO) << "Done loading to useridIndex_!\n"
               << "Since recommender construction, after loading to useridIndex took total: " << t.elapsed() << " seconds.\n";
     
     //rate the useritemRatingMatrix_; second run
     pCassPool_->load_from_user_trail(query_from_user_behavior_item.c_str(),
		                      useridIndex_,
				      itemidIndex_,
				      userItemRatingMatrix_);

 
     //std::cout << "Finished loading from user trail db!\n"
               //<< "Since recommender construction, after loading to userItemRatingMatrix_ took total: " << t.elapsed() << " seconds.\n";

     LOG(INFO) << "Finished loading from user trail db!\n"
               << "Since recommender construction, after loading to userItemRatingMatrix_ took total: " << t.elapsed() << " seconds.\n";

     // new itemid's index from Kafka streamed items; starting from 0
     new_item_index_ = 0;


     // DEBUG purposes
     size_t useridCount = useridIndex_.size();
     size_t itemidCount = itemidIndex_.size();
     
     //std::cout << useridCount << " users, " << itemidCount << " items!\n";
     LOG(INFO) << useridCount << " users, " << itemidCount << " items!\n";
     
     
     // Computing user average rating and load to userAvgRatingMap_ 
     for(int i = 0; i < useridCount; ++i)
     { 
	     score_t row_total = 0.0;
	     score_t row_avg_rating = 0.0;

	     for(int j = 0; j < itemidCount; ++j)
		     row_total += userItemRatingMatrix_[i][j];

	     try 
	     {
	        row_avg_rating = row_total / itemidCount;
             }
	     catch(...)
	     {
               continue;
	     }
	     //std::cout << "user " << i << " avg rating is: " << row_avg_rating << '\n';

	     userAvgRatingMap_[i] = row_avg_rating;
     }

     // for content-based similarity and user profiling
     
     std::string cont_usr_res_dir = "../src/utils/rs_content_profile/resource";

     //old version for pContAndUsrPrf_ module
     pContAndUsrPrf_.reset(new ProfileCreate(cont_usr_res_dir));
    
     //load to product titles's tokens 
     for(std::vector<std::string>::iterator it = productData_.begin(); it != productData_.end(); ++it)
     {
            std::vector<std::string> titleTokens;
            //std::cout << "Titles are: " << *it << "\n";

     	    pContAndUsrPrf_->segment(*it, titleTokens);

	    productTitleTokens_.push_back(titleTokens);
     }
 
     /* compute hyrbird item-item similarities, including
      * 1) itemCF similarity
      * 2) Content-based similarity
      */
     computeSimilarity_();
     
     //std::cout << "Since recommender construction, after computing the itemMutualRatingMatrix_ took total: " << t.elapsed() << " seconds.\n";
     LOG(INFO) << "Since recommender construction, after computing the itemMutualRatingMatrix_ took total: " << t.elapsed() << " seconds.\n";
     
     // loading item-item sreened&ranked results
     loadToitem2ItemRecsMap_(80);

     // flush results to disk for resuming purposes 
     flush_to_disk();
     
     //std::cout << "Since recommender construction, to finish construction took total: " << t.elapsed() << " seconds.\n";
     LOG(INFO) << "Since recommender construction, to finish construction took total: " << t.elapsed() << " seconds.\n";
}


// Constructor that uses local resource directory and cassandra
HotDealRecEngine::HotDealRecEngine(const std::string& cassandra_cluster_addr,
		                   const std::string& resource_dir)
{   
     boost::timer t;

     // only get frm hot_product_cqs and user_behavior_item_cqs within N days
     int time_span = 7;
     timeScreeningStr_ = izeneUtil::getNdaysPrevUTC(time_span);     
     //std::cout << "History time is: " << timeScreeningStr_ << "\n\n";
     LOG(INFO) << "History time is: " << timeScreeningStr_ << "\n\n";


     pCassPool_.reset(new izeneUtil::CassPool(cassandra_cluster_addr.c_str()));
    
     //std::cout << "Secondary Constructor called. resource_dir is: " << resource_dir << "\n";
     LOG(INFO) << "Secondary Constructor called. resource_dir is: " << resource_dir << "\n";

     std::ifstream inFile;
     std::string lineTmp;
     std::vector<std::string> lineTokens;

     //load to useridIndex_
     std::string userid_index_filepath = resource_dir + "/userid_index.tsv";
     inFile.open(userid_index_filepath.c_str());
     while(getline(inFile, lineTmp))
     {
         if(lineTmp.empty())
		 continue;
         
	 boost::split(lineTokens, lineTmp, boost::is_any_of("\t"));
	 if(lineTokens.size() != 2)
		 continue;
          
	 int index;

	 try
	 {
	     index = boost::lexical_cast<int>(lineTokens[0]);
	 }
	 catch(...)
	 {
             continue;
	 }

	 useridIndex_.insert(Indexer_t::value_type(index, lineTokens[1]));
     }

     inFile.close();

     //load to itemidIndex_
     std::string itemid_index_filepath = resource_dir + "/itemid_index.tsv";
     inFile.open(itemid_index_filepath.c_str());
     while(getline(inFile, lineTmp))
     {
         if(lineTmp.empty())
		 continue;
         
	 boost::split(lineTokens, lineTmp, boost::is_any_of("\t"));
	 if(lineTokens.size() != 2)
		 continue;
          
	 int index;

	 try
	 {
	     index = boost::lexical_cast<int>(lineTokens[0]);
	 }
	 catch(...)
	 {
             continue;
	 }

	 itemidIndex_.insert(Indexer_t::value_type(index, lineTokens[1]));
     }

     inFile.close();

     //load to itemMuMatrix_
     std::string item_mutual_matrix_filepath = resource_dir + "/item_mutual_matrix.tsv";
     inFile.open(item_mutual_matrix_filepath.c_str());
     while(getline(inFile, lineTmp))
     {
         // TODO: it should never be empty though
         if(lineTmp.empty())
	 {
	     //std::cout << "Empty line found in item_mutual_matrix!\n";
	     continue;
	 }

         boost::split(lineTokens, lineTmp, boost::is_any_of("\t"));
	 
	 std::vector<score_t> linePadding;
	 for(std::vector<std::string>::iterator it = lineTokens.begin(); it != lineTokens.end(); ++it)
	 {
	         score_t score = 0.0;
                
		 try
		 {
		      score = boost::lexical_cast<score_t>(*it);
		 }
		 catch(...)
		 {
		     continue;
		 }

		 linePadding.push_back(score);
	 }

	 itemMutualRatingMatrix_.push_back(linePadding);

     }

     inFile.close();


     //load to productTitleTokens_ and itemidIndexList_
     std::string product_title_tokens_filepath = resource_dir + "/product_title_tokens.tsv";
     inFile.open(product_title_tokens_filepath.c_str());
     int index_title = 0;
     while(getline(inFile, lineTmp))
     {
          if(lineTmp.empty())
	     continue;

	  boost::split(lineTokens, lineTmp, boost::is_any_of("\t"));
	  productTitleTokens_.push_back(lineTokens);

	  //load to itemidIndexList_
	  itemidIndexList_.push_back(index_title);

	  ++index_title;
     }
     
     inFile.close();

     //load to productIsListed_
     std::string product_islisted_filepath = resource_dir + "/product_islisted.tsv";
     inFile.open(product_islisted_filepath.c_str());
     while(getline(inFile, lineTmp))
     {
	  if(lineTmp.empty())
	     continue;

	  boost::split(lineTokens, lineTmp, boost::is_any_of("\t"));

	  int index; 
	  try
	  {
              index = boost::lexical_cast<int>(lineTokens[0]);
	  }
	  catch(...)
	  {
	      continue;
	  }

	  productIsListedStatus_[index] = (lineTokens[1] == std::string("1")) ? true : false;

     }

     // new itemid's index from Kafka streamed items; starting from 0
     new_item_index_ = 0;


     // DEBUG purposes
     size_t useridCount = useridIndex_.size();
     size_t itemidCount = itemidIndex_.size();
     
     //std::cout << useridCount << " users, " << itemidCount << " items!\n";
     LOG(INFO) << useridCount << " users, " << itemidCount << " items!\n";
     
     
     // Not needed in this contructor: 
     // 1) userAvgRatingMap_ 
     // 2) computeSimilarity_()
     // 3) flush_to_disk()
     
     // for content-based similarity and user profiling
     std::string cont_usr_res_dir = "../src/utils/rs_content_profile/resource";
     pContAndUsrPrf_.reset(new ProfileCreate(cont_usr_res_dir));
     
     // loading item-item sreened&ranked results
     loadToitem2ItemRecsMap_(80);

     //std::cout << "Finsihed recommender secondary construction in total: " << t.elapsed() << " seconds!\n";
     LOG(INFO) << "Finsihed recommender secondary construction in total: " << t.elapsed() << " seconds!\n";
}

void HotDealRecEngine::computeSimilarity_()
{
    boost::timer t;

    //std::cout << "Entering matrix computing!\n";
    LOG(INFO) << "Entering matrix computing!\n";

    const double item_cf_sim_weight = 0.5218;
    double item_content_sim_weight = 1 - item_cf_sim_weight;

    int itemidCount = itemidIndex_.size();
    int useridCount = useridIndex_.size();	    

    // init the itemMutualRatingMatrix_ 
    std::vector<score_t> linePaddings(itemidCount, 0.0);
    for(int i = 0; i < itemidCount; ++i)
	    itemMutualRatingMatrix_.push_back(linePaddings);

    //std::cout << "Done padding itemMutualRatingMatrix_!\n";
    LOG(INFO) << "Done padding itemMutualRatingMatrix_!\n";

    for(int item_row = 0;  item_row < itemidCount; ++item_row)
    {
         for(int item_col = 0; item_col < itemidCount; ++item_col)
	 {
             if(item_col == item_row)
		  continue;

	     // Symmetric; 
	     if(item_col < item_row)
	     {
                 itemMutualRatingMatrix_[item_row][item_col] = itemMutualRatingMatrix_[item_col][item_row];
		 continue;
	     }

	     /*********  itemCF calculation below **********/
	     // The actual computing of upper half 
             score_t numerator = 0.0;

	     // left half of the denominator
	     score_t denominator_1 = 0.0;

	     score_t denominator_2 = 0.0;

     
	     for(int user_row = 0; user_row < useridCount; ++user_row)
	     {

		 score_t user_row_avg = userAvgRatingMap_[user_row];
	         // std::cout << "User avg is: " << user_row_avg << '\n';
		     
                 numerator += (userItemRatingMatrix_[user_row][item_row] - user_row_avg) *
                              (userItemRatingMatrix_[user_row][item_col] - user_row_avg);
		 
		 denominator_1 += pow((userItemRatingMatrix_[user_row][item_row] - user_row_avg), 2);
		 denominator_2 += pow((userItemRatingMatrix_[user_row][item_col] - user_row_avg), 2);
	     }
	    

	     //TODO: to remove
	     score_t denominator_total;
	     
	     denominator_total = sqrt(denominator_1) * sqrt(denominator_2);
             
	     //std::cout << "denominator_total is: " << denominator_total << '\n';
                  



	     score_t item_cf_score_total = 0.0;
	    
	     if(denominator_total < -zero_thres || denominator_total > zero_thres)
	     {
	             item_cf_score_total = numerator / denominator_total; 
             }

	     // check exit as well 
             if(item_cf_score_total != item_cf_score_total)
	     {
                 //std::cout << "-nan caught! item_cf_score_total is: " << item_cf_score_total << "!\n";
	          
		 //force revert to 0.0
		 item_cf_score_total = 0.0;
	     }


	     itemMutualRatingMatrix_[item_row][item_col] = score_t (item_cf_score_total * item_cf_sim_weight); 
             
	     //std::cout << "item (" << item_row << "," << item_col << "): " << itemMutualRatingMatrix_[item_row][item_col] << "\n"; 

	     // new version of pContAndUsrPrf_ module only reuqire itemid_index 
	     score_t item_content_score = pContAndUsrPrf_->computeContentSim(productTitleTokens_[item_row], 
			                                                     productTitleTokens_[item_col]);

             //std::cout << "item_content_score is: " << item_content_score << "!\n";

             itemMutualRatingMatrix_[item_row][item_col] += score_t (item_content_score * item_content_sim_weight);
         
	 
	     //std::cout << "Hybrid: item (" << item_row << "," << item_col << "): " << itemMutualRatingMatrix_[item_row][item_col] << "\n"; 
	 //end of internal for loop
	 }
   
    // end of external for loop 	 
    }

    //std::cout << "Finished computing the itemMutualRatingMatrix_ in total: " << t.elapsed() << " seconds!\n";
    LOG(INFO) << "Finished computing the itemMutualRatingMatrix_ in total: " << t.elapsed() << " seconds!\n";
}


void HotDealRecEngine::loadToitem2ItemRecsMap_(int topKCount)
{
	
     int itemCount = itemidIndex_.size();

     for(int i = 0; i < itemCount; ++i)
     {
	     // Container for all <item,score> rating pairs 
	     RatingPairContainer_t item_rating_list;

	     // find the itemid for the one that need recommendation 
	     Indexer_t::left_const_iterator it_item_main = itemidIndex_.left.find(i);
             if(it_item_main == itemidIndex_.left.end())
		     continue;

	     std::string itemid_main_str = it_item_main->second;


	     for(int j = 0; j < itemCount; ++j)
	     { 
		  // avoid self-recommendation 
		  if(j == i)
	             continue;

		  // screen off the delisted items
		  if(!productIsListedStatus_[j])
                     continue;
         	
	     	  // find the itemid for the candidate
	          Indexer_t::left_const_iterator it_item_candidate = itemidIndex_.left.find(j);
                  if(it_item_candidate == itemidIndex_.left.end())
		     continue;

	          std::string itemid_candidate_str = it_item_candidate->second;

                  score_t item_candidate_score = itemMutualRatingMatrix_[i][j];

		  item_rating_list.push_back(std::make_pair(itemid_candidate_str,item_candidate_score));

	     // end of inner for loop
	     }

             std::string item_recomms;

	     item_recomms = getTopKFromRatingPairContainer_(item_rating_list, topKCount);

	     if(item_recomms.empty())
		   continue;

	     // insert entry to item2ItemRecsMap_
	     item2ItemRecsMap_.insert(boost::unordered_map<std::string, std::string>::value_type(itemid_main_str, item_recomms));

     // end of external loop 
     }



}


std::string HotDealRecEngine::get_recs_by_userid(const std::string& userid, int topKCount)
{

	boost::timer t;

	if(topKCount < 1)
	  return std::string("");

	//container for all <item,score> rating pairs, score is double  
	RatingPairContainer_t item_rating_list;
      
	// result from user-profile based item: <itemid,score> rating pairs, score is string too. item1:score1,item2:score2,...
        //std::vector<std::string> user_profile_rec_item_list;

	//Getting N days' purchased items from user_behavior_item_db in real-time 
	std::string query_get_user_history = "select itemid from products_keyspace.user_behavior_item_cqs where user_behavior_cq_userid = ";
	query_get_user_history += "\'" + userid + "\'";        
	query_get_user_history += " and ";
	
	// "id" is cassandra's built-in timestamp in timeuuid
	query_get_user_history += "id > maxTimeuuid(\'";
        query_get_user_history += timeScreeningStr_;
	query_get_user_history += "\') allow filtering";
            		
	std::vector<std::string> history_itemids;
       	bool db_success = pCassPool_->get_user_history(query_get_user_history.c_str(), history_itemids);

	//std::cout << "since entring get_recs_by_userid, after fetching userid from cassandra, took total: " << t.elapsed() << " seconds.\n";
	LOG(INFO) << "since entring get_recs_by_userid, after fetching userid from cassandra, took total: " << t.elapsed() << " seconds.\n";

        //Find userid's index in useridIndex_
	Indexer_t::right_const_iterator it_userid_index = useridIndex_.right.find(userid);



	/******** Case: using user profiling ****************************/
	if(it_userid_index == useridIndex_.right.end() ||
	   !db_success ||
           history_itemids.empty())
	{
            //std::cout << "user_profile for " << userid << ", called!\n";	

            // Read lock for new item list
            bool isLock;
            ScopedReadBoolLock Lock(mutex_, isLock);


	    RatingPairContainer_t item_rating_list;
        
	    std::string result;

	    std::vector<std::string> user_profile_rec_item_list_current;
            pContAndUsrPrf_->recommend(userid,
			               itemidIndexList_,
				       productTitleTokens_,  
			               user_profile_rec_item_list_current, 
	    			       topKCount);
       
	    std::vector<std::string> user_profile_rec_item_list_stream;
            pContAndUsrPrf_->recommend(userid,
			               itemidIndexListStream_,
				       productTitleTokensStream_,  
			               user_profile_rec_item_list_stream, 
	    			       topKCount);
            


	    if(user_profile_rec_item_list_current.empty() &&
	       user_profile_rec_item_list_stream.empty())
	    {
                //std::cout << "get_recs_by_userid took total: " << t.elapsed() << " seconds.\n";
                LOG(INFO) << "get_recs_by_userid for userid: " << userid << ", empty result, took total: " << t.elapsed() << " seconds.\n";
	        return std::string("");
	    }

            load_to_rating_pair_container_(user_profile_rec_item_list_current,
			                   itemidIndex_,
					   productIsListedStatus_,
			                   item_rating_list);
             
	    load_to_rating_pair_container_(user_profile_rec_item_list_stream,
			                   itemidIndexStream_,
					   productIsListedStatusStream_,
			                   item_rating_list);


	    result = getTopKFromRatingPairContainer_(item_rating_list, topKCount);

            //std::cout << "get_recs_by_userid took total: " << t.elapsed() << " seconds.\n";
            LOG(INFO) << "get_recs_by_userid for userid: " << userid << "took total: " << t.elapsed() << " seconds.\n" 
		      << "                       result: " << result << "\n";
 	    
	    return result;
         }
        /********* End of user profiling ****************************/

        int userid_index = it_userid_index->second;
 
	//std::cout << "userid_index for " << userid << " is: " << userid_index << '\n';



        // items that are in user history, must be in itemIndex too
        std::map<int, bool> isItemInHisotry;

	std::vector<int> history_itemid_indices;
	std::vector<int> potential_itemid_indices;


        //rating formula: base_score + (1- base_score) * browsedCount / Max_Browsed_Count 
	//1) base score for browsing once; 
	//2) browsed count as a weighted increment
	const score_t Base_score = 0.9;
	const score_t Max_Browsed_Count = 35;
		     
	// every visit gets an 1/Max_Browsed_Count score increment
	score_t visit_incremental_score = (1.0 - Base_score) / Max_Browsed_Count;
	

	for(std::vector<std::string>::iterator it = history_itemids.begin();
			                       it != history_itemids.end();
					       ++it)
	{  	
	      // find the userid's index: string -> int
	      Indexer_t::right_const_iterator it_itemid_index = itemidIndex_.right.find(*it);    
              
	      if(it_itemid_index == itemidIndex_.right.end())
		    continue;  

              int itemid_index = it_itemid_index->second;

                           //dedup
              if(isItemInHisotry[itemid_index])
                    continue;

              isItemInHisotry[itemid_index] = true;

               //Instant scoring 
	      score_t previous_score = userItemRatingMatrix_[userid_index][itemid_index];
	      if(previous_score < Base_score)
		      userItemRatingMatrix_[userid_index][itemid_index] = Base_score + visit_incremental_score;
	      /*
              else
	      {
		      userItemRatingMatrix_[userid_index][itemid_index] += visit_incremental_score;

		      // capping score to 1.0
		      if(userItemRatingMatrix_[userid_index][itemid_index] > 1.0)
			      userItemRatingMatrix_[userid_index][itemid_index] = 1.0;
	      }
              */


	      //saving to history
	      history_itemid_indices.push_back(itemid_index);
	}

	// parting itemidIndex to history and potential 
	for(Indexer_t::left_const_iterator it = itemidIndex_.left.begin();
			                   it != itemidIndex_.left.end();
					   ++it)
	{
              int itemid_index = it->first;
	    
	      if(!isItemInHisotry[itemid_index])     
		    potential_itemid_indices.push_back(itemid_index);

	}


	// Compute predictor score 
	for(std::vector<int>::iterator it_po = potential_itemid_indices.begin();
			               it_po != potential_itemid_indices.end();
				       ++it_po)
	{

	     // isListed is a prerequisite for a candidate's elegibility 
             if(!productIsListedStatus_[*it_po])
	        continue;

	     score_t numerator = 0.0;
	     score_t denominator = 0.0;



	     for(std::vector<int>::iterator it_h = history_itemid_indices.begin();
			                  it_h != history_itemid_indices.end();
					  ++it_h)
	     {
		     numerator += itemMutualRatingMatrix_[*it_po][*it_h] *
			          userItemRatingMatrix_[userid_index][*it_h];

		     denominator += itemMutualRatingMatrix_[*it_po][*it_h];
	     } 

	     //predictor score for each potential items
	     score_t item_predicted_score = 0.0;

	     if(denominator < -zero_thres || denominator > zero_thres)
	     {
		     item_predicted_score = numerator / denominator;
	     }

	     if(item_predicted_score != item_predicted_score)
	     {
		     //std::cout << "prediction score nan caught! item_predicted_score was " << item_predicted_score << "\n";

		     //force revert to 0.0
		     item_predicted_score = 0.0;
	     }

	     //find this potential item's itemid 
	     Indexer_t::left_const_iterator it_po_itemid = itemidIndex_.left.find(*it_po);

	     if(it_po_itemid == itemidIndex_.left.end())
		     continue;

	     std::string itemid_po = it_po_itemid->second;

	     item_rating_list.push_back(std::make_pair(itemid_po, item_predicted_score));

	}

	// result to return 
	std::string result_final;

        // get sorted DESC, topK 
        result_final = getTopKFromRatingPairContainer_(item_rating_list, topKCount);

        //std::cout << "get_recs_by_userid took total: " << t.elapsed() << " seconds.\n";
        LOG(INFO) << "get_recs_by_userid for userid: " << userid << ", took total: " << t.elapsed() << " seconds.\n" 
		  << "                       result: " << result_final << "\n";
 
	return result_final;


}


std::string HotDealRecEngine::get_recs_by_itemid(const std::string& itemid, int topKCount)
{

	if(topKCount < 1)
	     return std::string("");

	boost::timer t;

	std::string result_raw = item2ItemRecsMap_[itemid];

	// Not Found in current itemidIndex_; to try only content-based sim, based on current index and stream index 
	if(result_raw.empty())
	     return getPureContentSimItemRec_(itemid, topKCount);

	std::vector<std::string> item_score_pairs;

	boost::split(item_score_pairs, result_raw, boost::is_any_of(","));
     
	if(topKCount >= item_score_pairs.size())
	{

               // std::cout << "get_recs_by_itemid took total: " << t.elapsed() << " seconds.";

    		LOG(INFO) << "get_recs_by_itemid for itemid: " << itemid << ", took total: " << t.elapsed() << " seconds.\n" 
		          << "                       result: " << result_raw << "\n";
 
		return result_raw;
	}
	// scoped topK
	std::string result;
	for(std::vector<std::string>::iterator it = item_score_pairs.begin();
			                       it < item_score_pairs.begin() + topKCount;
					       ++it)
	{
                result += *it;
		result += ",";
	}

        result = result.substr(0, result.size() - 1);

        //std::cout << "get_recs_by_itemid took total: " << t.elapsed() << " seconds.\n";
    	LOG(INFO) << "get_recs_by_itemid for itemid: " << itemid << ", took total: " << t.elapsed() << " seconds.\n" 
		  << "                       result: " << result << "\n";
 
	return result;

}


std::string HotDealRecEngine::getTopKFromRatingPairContainer_(RatingPairContainer_t& item_rating_list, int topKCount)
{
             if(item_rating_list.empty())
		     return std::string("");

             std::sort(item_rating_list.begin(), item_rating_list.end(), PairSort<std::string, score_t>::sortDescendBySecond);

	     int pairCount = item_rating_list.size();

	     int topK;

	     if(topKCount > pairCount)
		  topK = pairCount;
	     else 
		  topK = topKCount;

	     std::string item_recomms;

             for(RatingPairContainer_t::iterator it = item_rating_list.begin();
			                         it != item_rating_list.begin() + topK;
						 ++it)
	     {
		  std::string score_str;

		  try
		  {
                    score_str = boost::lexical_cast<std::string>(it->second);

		  }
                  catch(...)
		  {
                     continue;
		  }

		  if(score_str.empty())
	             continue;

		  item_recomms += it->first;
		  item_recomms += ":";


		  item_recomms += score_str;
	          item_recomms += ",";

	     }

	     if(item_recomms.empty())
		   return std::string("");
 
             item_recomms = item_recomms.substr(0, item_recomms.size() - 1);


             return item_recomms;

}

// Load to newitem's index and titletokens; in exact same order 
bool HotDealRecEngine::insert_new_item(const std::string& itemid, const std::string& title)
{
	if(itemid.empty() || title.empty())
		return false;
	boost::timer t;

	//std::cout << "New item:" << itemid << "\t with tilte: " << title << " inserted!\n";
	LOG(INFO) << "New item:" << itemid << "\t with tilte: " << title << " inserted!\n";

	// starts from index 0
	itemidIndexStream_.insert(Indexer_t::value_type(new_item_index_, itemid));

        std::vector<std::string> titleTokens;
        pContAndUsrPrf_->segment(title, titleTokens);
	productTitleTokensStream_.push_back(titleTokens);

	// add to itemidList too
	itemidIndexListStream_.push_back(new_item_index_);

	// init status
        productIsListedStatusStream_[new_item_index_] = true;

	++new_item_index_;

	//std::cout << "insert_new_item: " << itemid << " , took total: " << t.elapsed() << " seoconds.\n"; 
	LOG(INFO) << "insert_new_item: " << itemid << " , took total: " << t.elapsed() << " seoconds.\n"; 

	return true;
}

// update both current and stream product islisted status; existed entry presumed 
void HotDealRecEngine::update_listed_status(const std::string& itemid)
{

     //std::cout << itemid << " listed status updated!\n";
     LOG(INFO) << itemid << " listed status updated!\n";

     boost::timer t;

     Indexer_t::right_const_iterator it = itemidIndex_.right.find(itemid);
     
     if(it != itemidIndex_.right.end())
     {
	   int i = it->second;
	   productIsListedStatus_[i] = false;
     }


     //update stream itemid's status
     Indexer_t::right_const_iterator itr = itemidIndexStream_.right.find(itemid);
     if(itr != itemidIndexStream_.right.end())
     {
           int j = itr->second;
	   productIsListedStatusStream_[j] = false;
     }

     //std::cout << "update_listed_status of itemid: " << itemid << ", took total: " << t.elapsed() << " seconds.\n";
     //LOG(INFO) << "update_listed_status of itemid: " << itemid << ", took total: " << t.elapsed() << " seconds.\n";

     return;
}

// pure content-based sim computing and recommendation 
std::string HotDealRecEngine::getPureContentSimItemRec_(const std::string& itemid, int topKCount)
{
     if(itemid.empty())
        return std::string("");	     


     //std::cout << "Pure Content Sim Item Scoring called!\n";
     LOG(INFO) << "Pure Content Sim Item Scoring called!\n";

     boost::timer t;

     // Read lock for new item list
     bool isLock;
     ScopedReadBoolLock Lock(mutex_, isLock);


     Indexer_t::right_const_iterator it_stream = itemidIndexStream_.right.find(itemid);

     // If it's not indexed in the new streamitemIndex then it doesn't deserve a recommendation 
     if(it_stream == itemidIndexStream_.right.end())
	return std::string("");     
     int itemid_index = it_stream->second;

     std::vector<std::string> itemid_title_tokens = productTitleTokensStream_[itemid_index];

     RatingPairContainer_t item_rating_list;


     // from current itemidIndex_
     for(Indexer_t::left_const_iterator it = itemidIndex_.left.begin(); it != itemidIndex_.left.end(); ++it)
     {
          int index = it->first; 
	  score_t score = pContAndUsrPrf_->computeContentSim(itemid_title_tokens, productTitleTokens_[index]); 

          item_rating_list.push_back(std::make_pair(it->second, score));

     }

     // from streamed itemidIndex_
     for(Indexer_t::left_const_iterator it = itemidIndexStream_.left.begin(); it != itemidIndexStream_.left.end(); ++it)
     {
         int index = it->first;
	 score_t score = pContAndUsrPrf_->computeContentSim(itemid_title_tokens, productTitleTokensStream_[index]);

         item_rating_list.push_back(std::make_pair(it->second, score));
     }

     std::string result = getTopKFromRatingPairContainer_(item_rating_list, topKCount);  

     //std::cout << "getPureContentSimItemRec_ for itemid: " << itemid << ", topK: " << topKCount 
	       //<< " took total: " << t.elapsed() << " seconds.\n";
     
     LOG(INFO) << "getPureContentSimItemRec_ for itemid: " << itemid << ", topK: " << topKCount 
	       << " took total: " << t.elapsed() << " seconds.\n";
 
     return result;
}


//rating_pairs: item1:score,item2:scor2
bool HotDealRecEngine::load_to_rating_pair_container_(const std::vector<std::string>& rating_pairs, 
		                    const Indexer_t& itemid_indexer,
				    std::map<int, bool>& product_status,
				    RatingPairContainer_t& rating_pair_container)
{
     if(rating_pairs.empty())
	     return false;

     // Translate the itemid_index to itemid and repack the result  
     for(std::vector<std::string>::const_iterator it = rating_pairs.begin();
		                                  it != rating_pairs.end();
						  ++it)
     {
	     // itemid_index:score
	     std::string itemid_index_score_pair = *it;

	     if(itemid_index_score_pair.empty())
		   continue;

             std::vector<std::string> pairTokens;
             boost::split(pairTokens, itemid_index_score_pair, boost::is_any_of(":"));

             if(pairTokens.size() != 2)
	           continue;

             int itemid_index; 	    
             try
	     {

                itemid_index = boost::lexical_cast<int>(pairTokens[0]);
	     }
	     catch(...)
             {
		continue;
	     }
 
	     score_t score;
             try
	     {
	        score = boost::lexical_cast<score_t>(pairTokens[1]);
             }
             catch(...)
	     {
	        continue;
             }		
	   
             // check if the item is still listed/on shelf 
             if(!product_status[itemid_index])
	        continue;
	
	     //find itemid
             Indexer_t::left_const_iterator it_i = itemid_indexer.left.find(itemid_index);
             if(it_i == itemid_indexer.left.end()) 
                continue;

             std::string itemid_str = it_i->second;
	
     
	     rating_pair_container.push_back(std::make_pair(itemid_str, score));
	    
       }


     return true;
}
