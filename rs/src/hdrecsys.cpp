/*************************************************************************
    @ File Name: hdrecsys.cpp
    @ Purpose: An HTTP service for providing recommendations for hotdeal products 
   
    @ Based on framework by Jerry Shi (Mail: jerryshi0110@gmail.com) 

    @ Last modified by: YuanChengzhi (Mail: yuanchengzhi@b5m.com)
    @ Modifcation time: 11:22am, May 27, 2016
    @ Optimizations : 2016-06-23 14:21:30
 ************************************************************************/

#include <sys/types.h>
#include <signal.h>
#include "HotDealRecEngine.h"

#include "mongoose.h"
#include "utils/json/json.h"

#include "utils/common.h"
#include "utils/GlogHelper.h"
#include "utils/FileSysHelper.h"
#include "utils/JsonPacker.h"

// TimingHelper and concurrent_queue headers are included 
#include "utils/kafka_driver.h"

boost::shared_ptr<izenelib::util::concurrent_queue<std::string> >  que(new izenelib::util::concurrent_queue<std::string>(80000));


// Initiliazing the recommender via 2 different strategies 
std::string cassandra_cluster_addr = "10.30.97.141,10.30.97.142,10.30.97.143,10.30.97.144,10.30.97.145";        

//HotDealRecEngine HDRec;
boost::shared_ptr<HotDealRecEngine> pHDRec;

void initHDRecEngine()
{

     //disable starting from disk for now 	  
     pHDRec.reset(new HotDealRecEngine(cassandra_cluster_addr));
     
     // Load recent resources from file for breakpoint start
     /*
     std::string res_master_folder = "../resource/";
     std::string most_recent_subfolder = izeneUtil::getLastModifiedSubFolder(res_master_folder);

     if(most_recent_subfolder.empty())
     {
          std::cout << "Empty resource sub folder list, using recommender default constructor!\n";
          LOG(INFO) << "Empty resource sub folder list, using recommender default constructor!\n";
        
	  pHDRec.reset(new HotDealRecEngine(cassandra_cluster_addr));

          std::cout << "finished init-swapping!\n";	  
          LOG(INFO) << "finished init-swapping!\n";	  
     }
     else 
     {
          std::cout << "Found a most recent subfolder from resource list, using secondary recommender constructor!\n";
          LOG(INFO) << "Found a most recent subfolder from resource list, using secondary recommender constructor!\n";
    
          std::string res_folder = res_master_folder + most_recent_subfolder;
          std::cout << " res folder for secondary recommender is: " << res_folder << "\n";
          LOG(INFO) << " res folder for secondary recommender is: " << res_folder << "\n";
     
          pHDRec.reset(new HotDealRecEngine(cassandra_cluster_addr, res_folder));

          std::cout << "finished init-swapping!\n";	  
          LOG(INFO) << "finished init-swapping!\n";	  
     }

     */
}

// Thread 1 for updating the matrix computation
void updateHDRecEngine()
{
     while(1)
     {
 	// 15 mins 
	boost::this_thread::sleep(boost::posix_time::seconds(900));
    
        //std::cout << "Started periodic updating hotdeal recommender!\n";
        LOG(INFO) << "Started periodic updating hotdeal recommender!\n";
        pHDRec.reset(new HotDealRecEngine(cassandra_cluster_addr));

     }
}

// Thread 2 for processing the new items
void new_item_processor()
{
     // new product data from concurrent_queue, pushed by kafka streamer
     while(1)
     {
         std::string data;
         que->pop(data);
        
	 //std::cout << que->size() << " new items, processing:\t" << data << "\n\n";
         if( que->size() > 1000)
	    LOG(INFO) << que->size() << " new items left in concurrent_queue. \n";

	 if(data.empty())
             continue;

	 Json::Reader jreader;
	 Json::Value jval;

	 if(jreader.parse(data, jval))
	 {
             if(jval["DOCID"] == Json::nullValue || 
	        jval["Title"] == Json::nullValue ||
		jval["opType"] == Json::nullValue)
		     continue;

             // "I" or "D" 
	     std::string opType = jval["opType"].asString();
	     std::string itemid = jval["DOCID"].asString();
	     std::string title = jval["Title"].asString();

	     //std::cout << "opType: " << opType << ", \titemid: " << itemid << ", \ttitle: " << title << "\n\n";
	     // LOG(INFO) << "opType: " << opType << ", \titemid: " << itemid << ", \ttitle: " << title << "\n\n";
	     
	     if(opType == std::string("D"))
	     {
	         pHDRec->update_listed_status(itemid);	 
               
	         LOG(INFO) << "Delete: opType: " << opType << ", \titemid: " << itemid << ", \ttitle: " << title << "\n\n";
		 continue;
	     }
	    
	     if(opType == std::string("I"))
	     {
                 if(!pHDRec->is_item_new(itemid))
                    continue;

	         LOG(INFO) << "Update: opType: " << opType << ", \titemid: " << itemid << ", \ttitle: " << title << "\n\n";
		 
		 pHDRec->insert_new_item(itemid, title);                 
	     }

         // end of json parser
	 }

     // end of while(1)
     }
}

// Thread 3 for getting data from kafka stream
void kafka_streamer()
{
     std::string conf_file = "../conf/consumer.conf";
     std::string work_mode = "consumer";
   
     LOG(INFO) << "Kafka_streamer process called!\n";

     //buit-in while(1) looping for kafka consumer 
     boost::shared_ptr<KafkaDriver> consumer(new KafkaDriver(conf_file, work_mode, que));

     // clean up when consumer finishes
     signal(SIGINT, sigterm);
     signal(SIGTERM, sigterm);

     RdKafka::wait_destroyed(5000);

     std::cout << "Kafka consumer exited!\n";

}


static int ev_handler(struct mg_connection *conn, enum mg_event ev)
{
	
	std::string docid;
        std::string userid;

        std::string topk("");
        const int DefaultTopK = 10;

	std::string JsonRes;
        std::string FinalRes;
	switch(ev)
	{
		case MG_AUTH: return MG_TRUE;
		case MG_REQUEST:
		{
			bool succ = true;
			char buffer_docid[65535];
                        char buffer_userid[65535];
			char buffer_topk[128];
			try{
			
				//GET METHOD
				if(mg_get_var(conn,"userid", buffer_userid, sizeof(buffer_userid)) > 0)
				{
                                        if(mg_get_var(conn, "topk", buffer_topk, sizeof(buffer_topk)) > 0)
                                        {
                                           std::string tvalue(buffer_topk);
					   topk = tvalue;
				        }

                                        std::string uvalue(buffer_userid);
                                        userid = uvalue;  					
                                 
				}
				else if(mg_get_var(conn,"docid", buffer_docid, sizeof(buffer_docid)) > 0)
				{
                                        if(mg_get_var(conn, "topk", buffer_topk, sizeof(buffer_topk)) > 0)
                                        {
                                           std::string tvalue(buffer_topk);
					   topk = tvalue;
				        }


                                        std::string dvalue(buffer_docid);
					docid = dvalue;

				}


				// POST. TODO
				else if(strcmp(conn->request_method,"POST") == 0){
					std::string content(conn->content,conn->content_len);
					docid = content;
				}
			}
		       	catch(std::exception& ex)
			{
				LOG(ERROR) << ex.what();
				//std::cerr << ex.what() << std::endl;
				succ = false;
			}

		
			//get results
			if(succ) 
			{

                            int topk_n;

		            if(!topk.empty())
		            {
			       try
			       {
		                   topk_n = boost::lexical_cast<int>(topk);
			       }
			       catch(...)
			       {
		         	   topk_n = DefaultTopK;
                                   LOG(ERROR) << "getting topk from url, and lexical_cast failed!\n";
			       }
		            }
			    else
			    {
			        topk_n = DefaultTopK;
			    }


			    if(!docid.empty())
			    {


				  JsonRes += "\"recs_by_item\":[";
				  
				  std::string rawRes = pHDRec->get_recs_by_itemid(docid, topk_n);
				  JsonRes += izeneUtil::jsonPacker("itemid", "score", rawRes);
				  JsonRes += "]";
				  
			    }
			    else if(!userid.empty())
			    {     
				  JsonRes += "\"recs_by_user\":[";
				  
				  std::string rawRes = pHDRec->get_recs_by_userid(userid, topk_n);
				  JsonRes += izeneUtil::jsonPacker("itemid", "score", rawRes);
				  JsonRes += "]";

			    }	  
			}
                
			FinalRes += "{" + JsonRes + "}";
			mg_send_data(conn,FinalRes.c_str(),FinalRes.length());
			//mg_printf_data(conn,JsonRes.c_str(),JsonRes.length());

			return MG_TRUE;
		}
		default: return MG_FALSE;
	}
}

static void *serve(void *server)
{
	for(;;)
	    mg_poll_server((struct mg_server *)server, 1000);

	return NULL;
}

void start_http_server(int pool_size = 1)
{

	std::vector<struct mg_server*> servers;
	for(int p = 0; p < pool_size; ++p)
	{
		std::string name = boost::lexical_cast<std::string>(p);
		LOG(INFO) << "Created thread id :" << name;
		//std::cout << "Creat thread: " << name << std::endl;
		
		struct mg_server * server;
		server = mg_create_server((void*)name.c_str(), ev_handler);
		if(p == 0)
		{
			mg_set_option(server,"listening_port", "15376");
		}
		else
		{
			mg_copy_listeners(servers[0], server);
		}

		servers.push_back(server);
	}
	for(uint32_t p = 0; p < servers.size(); ++p)
	{
		struct mg_server* server = servers[p];
		mg_start_thread(serve,server);
	}
	sleep(3600*24*365*10);
}


int main(int argc , char** argv)
{
       
	initHDRecEngine();
        
	// Glog configs
        std::string cProgram = "HDRECSYS";
	std::string logDir = "../log";
	InitGlog(cProgram.c_str(),logDir.c_str());

	int thread_num = 22;
	
	std::cout << "Start program...\n";
	
	// Thread 1: get data from kafka stream,push them into concurrent_queue
        //tail -f from kafka and push to concurrent_queue
        boost::thread th_kafka_stream(kafka_streamer);
	th_kafka_stream.detach();

    // Thread 2: consumer data from concurrent queue
	//processing new items 
	const int NewItemProcessorCount = 2;
	std::vector<boost::shared_ptr<boost::thread> > new_item_processor_threads(NewItemProcessorCount);
	for(std::vector<boost::shared_ptr<boost::thread> >::iterator it = new_item_processor_threads.begin(); 
			                                             it != new_item_processor_threads.end();
						                     ++it)
	{
		(*it).reset(new boost::thread(new_item_processor));
		(*it)->detach();
	}

         
	//Periodically update hdrec essentials 
	boost::thread hdrec_updater(updateHDRecEngine);
	hdrec_updater.detach();


	while(1)
	{
		start_http_server(thread_num);

        }

	return EXIT_SUCCESS;
}
