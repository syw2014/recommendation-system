/*************************************************************************
    @ File Name: qautofill.cpp
    @ Method:
    @ Author: Jerry Shi
    @ Mail: jerryshi0110@gmail.com 
    @ Created Time: 2015年10月23日 星期五 10时28分11秒
 ************************************************************************/
#include <iostream>
#include <sys/types.h>
#include <signal.h>

#include "mongoose.h"
#include "evaluationEngine.h"
#include <boost/lexical_cast.hpp>
//
std::string cass_addr = "10.30.97.141,10.30.97.142,10.30.97.143,10.30.97.144,10.30.97.145";
EvaluationEngine pEvaluate(cass_addr);

static int ev_handler(struct mg_connection *conn, enum mg_event ev){

	std::string request;
	switch(ev){
		case MG_AUTH: return MG_TRUE;
		case MG_REQUEST:
		{
			bool succ = true;
			char buffer[65535];
            std::string result("{}");
            bool isGetList = false;
			try{
				//get
				if(mg_get_var(conn,"userid",buffer,sizeof(buffer)) > 0){
					std::string pvalue(buffer);
					request = pvalue;
					isGetList = false;
				} else if(mg_get_var(conn, "list", buffer, sizeof(buffer)) > 0) {
				    isGetList = true;
                }
                std::string pvalue(buffer);
                request = pvalue;
			} catch(std::exception& ex){
				std::cerr << ex.what() << std::endl;
				succ = false;
			}

			//get results
			if(succ && !isGetList && !request.empty()){
			   result =  pEvaluate.Evaluation(request);
			} else if(succ && isGetList && !request.empty()) {
			    pEvaluate.GetUserIdList(result);
            }

			mg_send_data(conn,result.c_str(),result.length());
			return MG_TRUE;
		}
		default: return MG_FALSE;
	}
}

static void *serve(void *server){
	for(;;) mg_poll_server((struct mg_server *)server, 1000);
	return NULL;
}

void start_http_server(int pool_size = 1){

	std::vector<struct mg_server*> servers;
	for(int p = 0; p < pool_size; ++p){
		std::string name = boost::lexical_cast<std::string>(p);
		std::cout << "Creat thread: " << name << std::endl;
		struct mg_server * server;
		server = mg_create_server((void*)name.c_str(), ev_handler);
		if(p == 0){
			mg_set_option(server,"listening_port", "14441");
		}else{
			mg_copy_listeners(servers[0], server);
		}

		servers.push_back(server);
	}
	for(uint32_t p = 0; p < servers.size(); ++p){
		struct mg_server* server = servers[p];
		mg_start_thread(serve,server);
	}
	sleep(3600*24*365*10);
}

int main(int argc , char* argv[])
{
	int thread_num = 22;
	std::cout << "Start program...\n";
	while(1)
	{
		start_http_server(thread_num);
	}

	return EXIT_SUCCESS;
}
