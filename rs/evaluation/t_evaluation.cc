/*************************************************************************
 @ File Name: evaluate.cc
 @ Method: 
 @ Author: Jerry Shi
 @ Mail: jerryshi0110@gmail.com
 @ Created Time: Tue 05 Jul 2016 04:08:49 PM CST
 ************************************************************************/
#include <iostream>
//#include "cass_driver.h"
#include "evaluationEngine.h"

int main(int argc, char* argv[])
{
   /* CassandraDriver* pCass = NULL;
    std::string cassandra_addr = "10.30.97.141,10.30.97.142,10.30.97.143,10.30.97.144,10.30.97.145";
    pCass = new CassandraDriver(cassandra_addr);
    std::string query;
    pCass->GetUserHistory();
    pCass->GetItemInfos();

    delete pCass;
    pCass = NULL;*/
    std::string cassandra_addr = "10.30.97.141,10.30.97.142,10.30.97.143,10.30.97.144,10.30.97.145";
    EvaluationEngine p(cassandra_addr);
    std::string userid = "eb4128bf6421495cace58f58166fe4bc";
    std::string result = "";
    //p.GetRecommendation(userid, result);
    result = p.Evaluation(userid);
    return 0;
}
