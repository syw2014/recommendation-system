/*************************************************************************
 @ File Name: cass_driver.h
 @ Method: 
 @ Author: Jerry Shi
 @ Mail: jerryshi0110@gmail.com
 @ Created Time: Fri 01 Jul 2016 02:58:13 PM CST
 ************************************************************************/
// A simple wrapper for cassandra cpp-driver, based on it can operator cassandra
// easily.
#ifndef CASS_DRIVER_H
#define CASS_DRIVER_H

#include <iostream>
#include "cassandra.h"
#include <time.h>
#include <boost/unordered_map.hpp>

// Type of one document stored in cassandra products_keyspace.hot_product_cqs
struct DocType {
    std::string docid; 
    std::string title;
    std::string original_category;
    std::string date;
    bool status;
    //std::string brand;
    //std::string producer_country;
    //std::string price;
    //std::string original_price;
    //std::string channel_source;
    //std::string hot_deal_category;
    //std::string platform;
    //std::string topic_tag;
    //std::string url;
    DocType()
      :docid(""), title(""), original_category(""), date(""),status(false){
    }
};

// Type for user behavior in cassandra products_keyspace.user_behavior_item_cqs
struct BehaviorType {
    std::string docid;
    std::string type; // behavior type,share, browse, collect, purchase
    std::string time; // behavior time
    int count;        // behavior times for this item
    
    BehaviorType()
      :docid(""),type(""),time(""),count(-1) {
      }
};

typedef boost::unordered_map<std::string, DocType> ItemDBType;
typedef boost::unordered_map<std::string, std::vector<BehaviorType> > UserHistoryType;

class CassandraDriver
{
    private:
        CassFuture* connect_future_; // 
        CassCluster* cluster_; 
        CassSession* session_;
        
        UserHistoryType historyData_; // user behavior history data
        ItemDBType itemDB_;           // item database
        std::vector<std::string> useridList_; // user id list fetch from behavior data

        // TODO: current cpp-driver not support block, this method will casue coredump
        // Execute query
        bool QueryExecute_(const std::string& query_statement, CassFuture* future) {
            if (query_statement.empty()) {
                std::cout << "[ERROR] query: " << query_statement << "is empty!\n";
                return false;
            }
            // build statement and execute query
            CassStatement* statement = cass_statement_new(query_statement.c_str(), 0);
            future = cass_session_execute(session_, statement);

            if (cass_future_error_code(future) != CASS_OK) {
                std::cout << "[ERROR] query: " << query_statement << "execute failed!\n";
                return false;
            }
            cass_future_wait(future);
            cass_statement_free(statement);
            return true;
        }

        // Get field value from one row
        std::string GetValueFromRow_(const CassRow* row, const char*  field_name){
            const CassValue* value = cass_row_get_column_by_name(row, field_name);
            const char* value_str;
            std::size_t value_length;
            cass_value_get_string(value, &value_str, &value_length);
            if (value_length == 0)
                return "null";
            std::string result(value_str, value_length);
            
            return result;
        }

    public:
        // Should specify the cluster ip address when construct driver object
        CassandraDriver(const std::string& cluster_addr) 
        :connect_future_(NULL), cluster_(NULL), session_(NULL) {
            if (cluster_addr.empty()) {
                std::cout << "Cassandra cluster is not exist!\n";
                return;
            }
            cluster_ = cass_cluster_new();
            session_ = cass_session_new();
            
            // Add contact points
            cass_cluster_set_contact_points(cluster_, cluster_addr.c_str());
            // Provide the cluster object as configuration to connect the session
            connect_future_ = cass_session_connect(session_, cluster_);

            if (cass_future_error_code(connect_future_) != CASS_OK) {
                std::cout << "Connect cassandra cluster failed!\n";
                cass_future_free(connect_future_);
                cass_cluster_free(cluster_);
                cass_session_free(session_);
            }
        }

        ~CassandraDriver() {
            cass_future_free(connect_future_);
            cass_cluster_free(cluster_);
            cass_session_free(session_);
            
            connect_future_ = NULL;
            cluster_ = NULL;
            session_ = NULL;
        }
      
        // get item detail info
        void GetItemInfoByDocid(const std::string& docid
                        ,DocType& doc) {
            if (docid.empty())
                return;
            ItemDBType::iterator itemIter;
            itemIter = itemDB_.find(docid);
            if (itemIter != itemDB_.end()) {
                doc = itemIter->second;
            } else {
                return;
            }
        }

        // get user history data
        void GetHistoryByUserId(const std::string& userid
                        ,std::vector<BehaviorType>& behavior) {
            if (userid.empty())
                return;
            UserHistoryType::iterator historyIter;
            historyIter = historyData_.find(userid); 
            if (historyIter != historyData_.end()) {
                behavior = historyIter->second;
            } else {
                return;
            }
        }

        // get user id list
        void GetUserIdLists(std::vector<std::string>& idList) {
            idList = useridList_;
        }

        // Get product details from db, title, category
        bool GetItemInfos() {
            
           // std::string query_statement = "select * from products_keyspace.hot_product_cqs ";
            std::string query_statement =  "SELECT docid,original_category,status,title,writetime(title) FROM products_keyspace.hot_product_cqs";
            if (query_statement.empty()) {
                std::cout << "[ERROR] " << query_statement << "is empty!\n";
                return false;
            }
            // Execute query
            CassFuture* future;
            CassStatement* statement = cass_statement_new(query_statement.c_str(), 0);
            future = cass_session_execute(session_, statement);
            
            if (cass_future_error_code(future) == CASS_OK) {
                const CassResult* result = cass_future_get_result(future);
                // Extract all fields from tables
                CassIterator* rows = cass_iterator_from_result(result);
                while (cass_iterator_next(rows)) {
                
                const CassRow* row = cass_iterator_get_row(rows);
                // get docid
                std::string docid = GetValueFromRow_(row, "docid");
                // get title
                std::string title = GetValueFromRow_(row, "title");
                // get category
                //std::string category = GetValueFromRow_(row, "original_category");
                const CassValue* val = cass_row_get_column_by_name(row, "original_category");
                const char* value_str;
                std::size_t value_length;
                cass_value_get_string(val, &value_str, &value_length);
                std::string category(value_str, value_length);
                // get status
                const CassValue* stat = cass_row_get_column_by_name(row, "status");
                cass_bool_t status;
                cass_value_get_bool(stat, &status);
               // TODO
                // get write time
                const CassValue* value = cass_row_get_column_by_name(row, "writetime(title)");
                cass_int64_t id;
                cass_value_get_int64(value, &id);
                time_t time = id/1000000;
                char buff[20];
                strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&time));
                std::string writeTime(buff);
                
                // Construct history data
                ItemDBType::iterator itemIter;
                itemIter = itemDB_.find(docid);
                if (itemIter == itemDB_.end()) {
                    DocType doc;
                    doc.docid = docid;
                    doc.title = title;
                    doc.original_category = category;
                    doc.date = writeTime;
                    doc.status = status;
                    itemDB_[docid] = doc;
                }
               // std::cout << "TEST :" << docid << "\t" << title << "\t" << status
               //         << "\t" << category << "\t"  << writeTime <<std::endl;
                }
                cass_iterator_free(rows); 
                cass_result_free(result);
            }
           std::cout << "TEST: item database size: " << itemDB_.size() << std::endl;

            cass_statement_free(statement);
            cass_future_free(future);
            return true;
        }

        // Get user history from db, docid, time, behavior type
        bool GetUserHistory() {
            std::string query_statement = "select user_behavior_cq_userid,behavior_type,id,itemid,writetime(itemid) from products_keyspace.user_behavior_item_cqs ";
            if (query_statement.empty()) {
                std::cout << "[ERROR] " << query_statement << "is empty!\n";
                return false;
            }
            // Execute query
            CassFuture* future;
           /* if (!QueryExecute_(query_statement, future)) {
                std::cout << "[ERROR] query execute failed!\n";
                return false;
            }*/
            
            CassStatement* statement = cass_statement_new(query_statement.c_str(), 0);
            future = cass_session_execute(session_, statement);
            useridList_.clear(); 
            if (cass_future_error_code(future) == CASS_OK) {
                const CassResult* result = cass_future_get_result(future);
                // Extract all fields from tables
                CassIterator* rows = cass_iterator_from_result(result);
                while (cass_iterator_next(rows)) {
                // get userid
                const CassRow* row = cass_iterator_get_row(rows);
                std::string userid = GetValueFromRow_(row, "user_behavior_cq_userid");
 
                // get itemid
                std::string itemid = GetValueFromRow_(row, "itemid");

               // TODO
               // std::string uuidstr = GetValueFromRow_(row, "id");
               // CassUuid uuid;
               // cass_uuid_from_string(uuidstr.c_str(), &uuid);
               // cass_uint64_t timestamp = cass_uuid_timestamp(uuid);
                // get write time
                const CassValue* value = cass_row_get_column_by_name(row, "writetime(itemid)");
                cass_int64_t id;
                cass_value_get_int64(value, &id);
                time_t time = id/1000000;
                char buff[20];
                strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&time));
                std::string writeTime(buff);
                // get behavior type
                std::string type = GetValueFromRow_(row, "behavior_type");

                // Construct history data
                BehaviorType history;
                UserHistoryType::iterator historyIter;
                history.docid = itemid;
                history.type = type;
                history.time = writeTime;
                history.count = 1;
                // check userid in map
                historyIter = historyData_.find(userid);
                if (historyIter != historyData_.end()) {
                    historyIter->second.push_back(history);
                } else {
                    std::vector<BehaviorType> behavior;
                    behavior.push_back(history);
                    historyData_[userid] = behavior;
                    useridList_.push_back(userid);
                }
               // std::cout << "TEST :" << userid << "\t" << itemid << "\t" 
               //     << type << "\t"  << buff <<std::endl;
               }
            
                cass_iterator_free(rows); 
                cass_result_free(result);
            }
            std::cout << "TEST: history size: " << historyData_.size() << std::endl;

            cass_statement_free(statement);
            cass_future_free(future);
            return true;
        }
};


#endif // cass_driver.h
