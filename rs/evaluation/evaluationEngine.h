/*************************************************************************
 @ File Name: evaluationEngine.h
 @ Method: 
 @ Author: Jerry Shi
 @ Mail: jerryshi0110@gmail.com
 @ Created Time: Wed 06 Jul 2016 03:48:25 PM CST
 ************************************************************************/
#ifndef EVALUATION_ENGINE_H
#define EVALUATION_ENGINE_H

#include <iostream>
#include <curl/curl.h>
#include "cass_driver.h"
#include "json/json.h"

std::string gResponse;
// TODO: optimizate
std::size_t pCallBack(char* str, std::size_t size, std::size_t nmemb, void* up) {
    for(std::size_t i = 0; i < size*nmemb; ++i)
        gResponse.push_back(str[i]);
    return size*nmemb;
}
class EvaluationEngine
{
    private:
        CassandraDriver* pCass_;

    public:
        EvaluationEngine(const std::string& cass_addr)
         :pCass_(NULL){
             if (cass_addr.empty()) {
                 std::cout << "Cassandra driver construct failed, cluster address " << cass_addr << std::endl;
                 return;
             }
             pCass_ = new CassandraDriver(cass_addr);
             // load data from cassandra
             pCass_->GetItemInfos();
             pCass_->GetUserHistory();
        }

        ~EvaluationEngine() {
            if(pCass_ != NULL) {
                delete pCass_;
                pCass_ = NULL;
            }
        }
        
        void GetUserIdList(std::string& userIdList) {
            std::vector<std::string> list;
            pCass_->GetUserIdLists(list);
            Json::Value Juserid, Jres;
            for (std::size_t i = 0; i < list.size(); ++i) {
                Juserid.append(list[i]);
            }
            Jres["userid_list"] = Juserid;
            userIdList = Jres.toStyledString();
            std::cout << "userIdList:" << userIdList << std::endl ;
        }

        std::string Evaluation(const std::string& userid) {
            std::string result("");
            if(userid.empty())
                return result;
            // get recommendation 
            std::string recRes("");
            GetRecommendation(userid, recRes);
        
            // extract docid
            Json::Reader reader;
            Json::Value jrecRes;
            Json::Value FinRes;
            Json::Value recomm;
            Json::Value history;
            if(!reader.parse(recRes, jrecRes, false)) {
                return result;
            }
            // get recommeded item info
            for (std::size_t i = 0; i < jrecRes["recs_by_user"].size(); ++i) {
                Json::Value info;
                info["docid"] = jrecRes["recs_by_user"][i]["itemid"];
                info["score"] = jrecRes["recs_by_user"][i]["score"];
                DocType doc;
                std::string itemid = info["docid"].asString();
                pCass_->GetItemInfoByDocid(itemid, doc);
                info["title"] = doc.title;
                info["status"] = doc.status;
                info["original_category"] = doc.original_category;
                info["date"] = doc.date;
                recomm.append(info);
            }
            // get user history
            std::vector<BehaviorType> behavior;
            pCass_->GetHistoryByUserId(userid, behavior);
            for (std::size_t j = 0; j < behavior.size(); ++j) {
                Json::Value info;
                info["docid"] = behavior[j].docid;
                info["type"] = behavior[j].type;
                info["time"] = behavior[j].time;
                info["count"] = behavior[j].count;
                DocType doc;
                pCass_->GetItemInfoByDocid(behavior[j].docid, doc);
                info["title"] = doc.title;
                info["status"] = doc.status;
                info["original_category"] = doc.original_category;
                info["date"] = doc.date;
                history.append(info);
            }
            // get item informations
            FinRes["recommendation"] = recomm;
            FinRes["history"] = history;

            result = FinRes.toStyledString();
            std::cout << "Test: " << result << std::endl;
            return result;
        }

        void GetRecommendation(const std::string& userid, std::string& result) {
            std::string url = "http://hdrec.b5msoft.com/?userid=";
            result = "";
            if (!userid.empty()) {
                url += userid;
            }
            gResponse = "";
            // get request
            try {
                CURL* curl;
                CURLcode ret;
                //struct curl_slist* headers = NULL;
                curl_global_init(CURL_GLOBAL_ALL);
                curl = curl_easy_init();


                // url
                curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
                curl_easy_setopt(curl, CURLOPT_TIMEOUT,2);
                curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &pCallBack);

                ret = curl_easy_perform(curl);
                if(ret != CURLE_OK)
                    std::cout << "failed!\n";
                curl_easy_cleanup(curl);
                curl_global_cleanup();
                
            } catch(std::exception& ex) {
                std::cout << "Send Exeception: " << ex.what() << std::endl;
            }
            result = gResponse;
            std::cout << "TEST: " << result << std::endl;
        }
};

#endif // evaluationEngine.h
